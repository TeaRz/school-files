class Rabbit {
    constructor(type, name){
        this.type = type;
        this.name = name;
    }
    speak(line){
        console.log(`The ${this.type} rabbit says ${line} their name is ${this.name}`);
    }
}
Rabbit.prototype.teeth = "small";
Rabbit.prototype.name = "name";
Rabbit.prototype.nose = "cute";

let killerRabbit = new Rabbit("killer");
let blackRabbit = new Rabbit("black");
let whiteRabbit = new Rabbit("white");

killerRabbit.name = "Lily";
killerRabbit.teeth = "Long, sharp, and bloody";
killerRabbit.speak("Doom....");
blackRabbit.name = "Smalls";
blackRabbit.speak("Hello");
whiteRabbit.name = "Bigs";
whiteRabbit.speak("Good Morning");

console.log("------");
console.log(`${killerRabbit.name} is the name of the ${killerRabbit.type} rabbit and they have ${killerRabbit.teeth} teeth`);
console.log(`${blackRabbit.name} os the name of the ${whiteRabbit.type} rabbit and they have ${whiteRabbit.teeth} teeth`);
console.log("------");
console.log("nose" in blackRabbit);
console.log("Bigs" in whiteRabbit);


//----------------------------------------------------------------------------------------------------------------------


class Rabbit {
	constructor(type){
    this.type = type;
    }
    toString(){
    return `${this.type} rabbit`;
    }
}
Rabbit.prototype.teeth = "small";

let killerRabbit = new Rabbit("killer");
let blackRabbit = new Rabbit("black");
killerRabbit.teeth = "Long, sharp, and bloody";

console.log("My new rabbit is a " + blackRabbit.toString());


//----------------------------------------------------------------------------------------------------------------------


class Temperature {
	constructor(celsius) {
    	this.celsius = celsius;
    }
  get  fahrenheit() {return this.celsius * 1.8 + 32; }
  set fahrenheit(value) {this.celsius = (value - 32) / 1.8;}
}

let temp = new Temperature(22);

temp.fahrenheit = 77;

console.log(temp.fahrenheit);
console.log(temp.celsius);
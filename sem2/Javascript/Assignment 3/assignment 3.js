let movies = [
    {
        title: "Castle in the Sky",
        rating:  8.1,
        year: "1986",
        genre: "Animation",
    },
    {
      title: "Grave of the Fireflies",
      rating: 8.5,
      year: "1988",
      genre: "Animation",  
    },
    {
        title: "My Neighbor Totoro",
        rating: 8.2,
        year: "1988",
        genre: "Animation",
    },
    {
      title: "Kiki's Delivery Service",
      rating: 7.9,
      year: "1989",
      genre: "Animation",  
    },
    {
        title: "Princess Mononoke",
        rating: 8.4,
        year: "1997",
        genre: "Animation",
    }];

console.log(movies.rating.sort(function(a, b){return a-b})); 

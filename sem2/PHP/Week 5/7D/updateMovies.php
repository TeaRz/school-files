<!DOCTYPE html>
<html>
    <head>
        <title>UPDATE Movies Database</title>
    </head>
    <body>
        <div>
            <h3>UPDATING DIRECTOR NAME in the Movies database</h3>
            <h4>Programmed by Cody Pedro</h4><br />
            
            <?php 
                require_once("connectvars.php");
                $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

                $TitleIN = trim($_POST['TitleIN']);
                $DirectorIN = trim($_POST['DirectorIN']);

                $query = "UPDATE Movies SET director = '$DirectorIN' WHERE title = '$TitleIN'";
                print("The query is: <b>$query</b><br /><br />");

                if(mysqli_query($dbc, $query)) {
                    echo "The UPDATE query was successful.";
                    print("<a href='../7C/displayMovies.php'>View Movies</a>");
                } else {
                    echo "The UPDATE query FAILED!" . mysqli_error($dbc);
                }

                mysqli_close($dbc);
            ?>
        </div>
    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <title>HTML form for Movie Delete</title>
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
        <div>
            <h2>Movie DELETE form - programmed by Cody Pedro</h2>
            
            <?php 
                //set the variables for the database access:
                require_once('connectvars.php');

                $dbc = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

                $query = "SELECT title from Movies";
                $result = mysqli_query ($dbc, $query);
                $result2 = mysqli_query($dbc, $query);
                
                echo "<p>This are movies in your database: </p>";
                echo "<ul>";
                while($Row = mysqli_fetch_array ($result2)) {
                    echo "<option value='$Row[title]'></option>";
                }
                echo "</datalist>";
            ?>
        <form action="deleteMovie.php" method="post">
            <label for="movies">Movie Title to be deleted from database:</label> 
            <input list="movies" name="TitleIN" size="20" />
            <br />
            <input type="submit" name="SUBMIT" value="Delete Record!" />
        </form>
    </div>
</body>
</html>
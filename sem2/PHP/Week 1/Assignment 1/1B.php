<!DOCTYPE html>
<html>
	<head></head>
	<body>
	<?php
		echo "<h2>Module! - Project 1: An HTML Color Table</h2>";
		echo "<table>";
		echo "<tr>";
		echo "<td>Blue</td>";
		echo "<td style=\"width:40px; background-color:#0000ff\"></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>Green</td>";
		echo "<td style=\"width:40px; background-color:#00ff00\"></td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td>Red</td>";
		echo "<td style=\"width:40px; background-color:#ff0000\"></td>";
		echo "</tr>";
		echo "</table>";
		echo "<p>Programmed by - \"Cody Pedro\"</p>";
		?>
	</body>
</html>
<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<?php 
			echo "<h2>Seven Natural Wonders of Canada</h2>";
			echo "<ul>";
			echo "<li>Niagara Falls, Ontario</li>";
			echo "<li>Body of Fundy, the Maritimes</li>";
			echo "<li>Rocky Mountains, British Columbia / Alberta</li>";
			echo "<li>Nahianni National Park Reserve, Northwest Territories</li>";
			echo "<li>Gros Morne National Park, Newfoundland</li>";
			echo "<li>Dinosaur Provincial Park, Alberta</li>";
			echo "<li>Northern Lights</li>";
			echo "</ul>";
			echo "<p><i>Images: </i></p>";
			echo "<img src='niagarafalls.jpg' />";
			echo "<img src='rockies.jpg' />";
			echo "<img src='northernlights.jpg' /><br />";
			echo '<p>Programmed by - "Cody Pedro"</p>';
		?>
	</body>
</html>

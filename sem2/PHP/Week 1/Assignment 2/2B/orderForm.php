<!DOCTYPE html>
<html>
	<head></head>
	<body>
		<h1>Company Spring Retreat</h1>
		<h2>Jacket Order: Confirmation</h2>
		<?php 
			$gender = $_POST['gender'];
			$size = $_POST['size'];
			$color = $_POST['color'];
			echo "<h3>For you, we will order a jacket in $color and size $size for a $gender employee.</h3>";
		?>
		<hr />
		<h2>Programmed by: Cody Pedro</h2>
	</body>
</html>
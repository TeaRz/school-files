<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
	</head>
	<body>
		<?php 
			$name = $_GET['name'];
			$live = $_GET['live'];
			
			if(empty($name)) {
				print "You did not enter a user name. <br />";
				print "Please return to the form and re-enter your information.";
			} else {
				if(empty($live)) {
					print "You did not enter where you live.<br />";
					print "Please return to the form and re-enter your information.";
				} else {
					print "Your name is $name <br /> You live in $live";
				}
			}
		?>
	</body>
</html>
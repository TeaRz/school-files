<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title></title>
	</head>
	<body>
		<?php 
			//This prigram receives the quantity from a form
			$quantity = $_POST['quantity'];
			$cost = 2000.00;
			$discount = 100.00;
			$tax = 0.13;
			
			if ($quantity < 1){
				echo "Please enter a valid quantity. <br />";
				echo "Please return to the form.";
			} else {
				$discount = abs($discount);
				$cost = abs($cost);
				$tax = $tax + 1;
				
				$totalCost = $cost * $quantity;
				
				if($totalCost < 5000.00) {
					echo "Discount will not apply as the sale is under $5,000.";
				} elseif($totalCost > 5000.00) {
					$totalCost = $totalCost - $discount;
				}
				$totalCost = $totalCost * $tax;
				$payment = round(($totalCost / 12) , 2);
				number_format($payment, 2);
			}
			
			echo "<h1>The PHP payment calculation conditionals program</h1><br />";
			echo "<h2>programmed by Cody Pedro</h2><br />";
			echo "<p>You requested to purchase $quantity widget(s) at \$$cost each.<br />";
			echo "<p>The total with tax, minus any discount, comes to " . number_format($totalCost, 2) . ".<br />";
			echo "<p>You may purchase the qidget(s) in 12 monthly installments of \$$payment each.<br />";
		?>
	</body>
</html>
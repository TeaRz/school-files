<!DOCTYPE html>
<html>
	<head>
		<title>Perimeter Calculation</title>
	</head>
	<body>
		<h1>Perimeter Calculation - by Cody Pedro</h1>
		<?php
		if(isset($_POST['submit'])){

			$name = $_POST['name'];
			$rlength = $_POST['length'];
			$rwidth = $_POST['width'];

			$perimeter = 0;

			// function definition
			function perim($rlength, $rwidth) {
				$perimeter = ($rlength * 2) + ($rwidth * 2);
			}
			echo "Hi $name,<br>";
			echo perim($rlength, $rwidth);

			echo "<br><hr>";
		} else {?>

			<form action="perimeter.php" method="post">
				Enter your Name:
				<input type="text" size="15" id="name" name="name"/><br/>
				Enter a Length:
				<input type="text" size="15" id="length" name="length"/><br/>
				Enter a Width:
				<input type="text" size="15" id="width" name="width"/><br/>
				<input type="submit" name="submit" value="Submit"/>
			</form>
		<?php
			}
		?>
	</body>
</html>
<DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Sorting Grades</title>
    </head>
    <body>
        <h2>sortGrades.php - Programmed by Cody Pedro</h2>
        <?php 
            //associative arrys i.e. $grades[Stephen=99;$grades[Luca]=62...
            $grades = array('Stephen' => 99, 'Luca' => 62, 'Will' => 92, 'Cole' => 87, 'Nathan' => 75, 'Jack' => 15);

            echo "Originally, the array looks like this: <br />";
            foreach($grades as $name => $grade) {
                echo "$name: $grade <br />";
            }
            echo "<br />";

            echo "After sorting the array by value using arsory, the array looks like this: <br />";
            arsort($grades);
            foreach($grades as $name => $grade) {
               echo "$name: $grade <br />";
            }
            echo "<br />";

            echo "After sorting ther array by key using ksort(), the array looks like this: <br />";
            ksort($grades);
            foreach($grades as $name => $grade){
               echo "$name: $grade <br />";
            }
            echo "<br />";
        ?>
    </body>
</html>
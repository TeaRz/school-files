<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Pizza Topping Selector</title>
    </head>
    <body style="background-color: rgb(227,243,209);">
        <h2>Pizza Topping Selector - by Cody Pedro</h2>
        You selected the following toppings: <br />
        <ul>
        <!-- Iterate over submitted pizza topping values
            and print them to the screen -->
        <?php 
            $opts = $_POST['toppings'];
            foreach($opts as $option) {
                echo "<li>$option </li>";
            }
        ?>
        </ul>
    </body>
</html>
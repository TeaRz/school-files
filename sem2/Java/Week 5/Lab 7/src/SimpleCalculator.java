import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.stage.Stage;

public class SimpleCalculator extends Application {
	private TextField textFieldNumber1 = new TextField();
	private TextField textFieldNumber2 = new TextField();
	private TextField textFieldResult = new TextField();
	private Button buttonAdd = new Button("Add");
	private Button buttonSubtract = new Button("Subtract");
	private Button buttonMultiply = new Button("Multiply");
	private Button buttonDivide = new Button("Divide");
	
	@Override
	public void start(Stage primaryStage) {
		HBox hbox1 = new HBox(5);
		HBox hbox2 = new HBox(5);
		VBox vbox = new VBox(15);
		hbox1.getChildren().addAll(new Label("Number1: "), textFieldNumber1, new Label("Number2: "), textFieldNumber2, new Label("Result: "), textFieldResult);
		hbox2.getChildren().addAll(buttonAdd, buttonSubtract, buttonMultiply, buttonDivide);
		vbox.getChildren().addAll(hbox1, hbox2);
		
		hbox1.setAlignment(Pos.CENTER);
		hbox2.setAlignment(Pos.CENTER);
		textFieldNumber1.setPrefColumnCount(4);
		textFieldNumber2.setPrefColumnCount(4);
		textFieldResult.setPrefColumnCount(4);
		vbox.setPadding(new Insets(2, 5, 0, 0));
		
		buttonAdd.setOnAction(e -> add());
		buttonSubtract.setOnAction(e -> subtract());
		buttonMultiply.setOnAction(e -> multiply());
		buttonDivide.setOnAction(e -> divide());
		
		Scene scene = new Scene(vbox);
		primaryStage.setTitle("Simple Calculator");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private void add() {
		textFieldResult.setText(String.valueOf(Double.parseDouble(textFieldNumber1.getText()) + Double.parseDouble(textFieldNumber2.getText())));
	}
	
	private void subtract() {
		textFieldResult.setText(String.valueOf(Double.parseDouble(textFieldNumber1.getText()) - Double.parseDouble(textFieldNumber2.getText())));
	}
	
	private void multiply() {
		textFieldResult.setText(String.valueOf(Double.parseDouble(textFieldNumber1.getText()) * Double.parseDouble(textFieldNumber2.getText())));
	}
	
	private void divide() {
		textFieldResult.setText(String.valueOf(Double.parseDouble(textFieldNumber1.getText()) / Double.parseDouble(textFieldNumber2.getText())));
	}
}
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class JavaIsFun extends Application {
	
	
	@Override
	public void start(Stage primaryStage) {
		StackPane pane = new StackPane();
		Text text1 = new Text("Java is fun");
		Text text2 = new Text("Java is Powerful");
		pane.getChildren().addAll(text1);
		
		pane.setOnMouseClicked(e -> {
			if (pane.getChildren().contains(text1)) {
				pane.getChildren().add(text2);
				pane.getChildren().remove(text1);
			}
			else {
				pane.getChildren().add(text1);
				pane.getChildren().remove(text2);
			}
		});
		
		Scene scene = new Scene(pane);
		primaryStage.setTitle("Java is Fun");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}

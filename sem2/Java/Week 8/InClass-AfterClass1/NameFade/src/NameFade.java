import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class NameFade extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Create a text to store our name
		Text name = new Text("Nicholas Sylvestre");
		
		//Create a new font object
		Font nameFont = Font.font("Times New Roman", FontWeight.BOLD, FontPosture.ITALIC, 40);
		
		//Set the font to the text obj
		name.setFont(nameFont);
		
		//Style the font color and stroke
		name.setFill(Color.MAROON);
		name.setStroke(Color.BLACK);
		
		//Create a fade transition
		//Set the duration and object its linked to
		FadeTransition fade = new FadeTransition(Duration.millis(1000), name);
		
		//set the "from" value (opacity)
		fade.setFromValue(1);
		
		//set the "to" value (opacity)
		fade.setToValue(0);
		
		//set the cycle count (how many times it should run)
		fade.setCycleCount(PathTransition.INDEFINITE);
		
		//Set auto reverse cycle to true
		fade.setAutoReverse(true);
		
		//play the animation
		fade.play();
		
		
		
		StackPane pane = new StackPane();
		pane.getChildren().add(name);
		Scene scene = new Scene(pane, 400, 200);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Name Fade Example");
		primaryStage.show();
		
	}

}







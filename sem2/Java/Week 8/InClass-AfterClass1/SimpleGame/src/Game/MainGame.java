package Game;

import Scenes.WelcomeScene;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainGame extends Application {
	public static Stage mainStage;
	
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Make the primaryStage global so any class can change scenes
		mainStage = primaryStage;
		mainStage.setScene(new WelcomeScene());
		mainStage.setTitle("Simple Game Example");
		mainStage.show();
	}

}

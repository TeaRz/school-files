package Panes;

import Game.MainGame;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.RotateTransition;
import javafx.animation.ScaleTransition;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class WelcomePane extends BorderPane {
	public WelcomePane() {
		//Create font object
		Font welcomeFont = Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 30);
		
		//Create new text object and apply font obj
		Text welcomeText = new Text("Steak & Berries");
		welcomeText.setFont(welcomeFont);
		
		//Apply color to the text obj
		welcomeText.setFill(Color.RED); 
		welcomeText.setStroke(Color.BLACK);
		
		//Create fade and scale to welcomeFont obj
		FadeTransition fade = new FadeTransition(Duration.millis(1000), welcomeText);
		fade.setFromValue(0.0);
		fade.setToValue(1);
		fade.setCycleCount(1);
		
		ScaleTransition scale = new ScaleTransition(Duration.millis(1000), welcomeText);
		scale.setByX(0.75);
		scale.setByY(0.75);
		
		
		//Create the bottom text
		Text authorText = new Text("Created By MAD200");
		authorText.setFont(welcomeFont);
		authorText.setFill(Color.RED);
		authorText.setStroke(Color.BLACK);
		authorText.setStrokeWidth(1);
		
		//Create a fade transition for the author text
		FadeTransition authorFade = new FadeTransition(Duration.millis(1000), authorText);
		authorFade.setFromValue(0);
		authorFade.setToValue(1);
		authorFade.setCycleCount(1);
		
		//Create a rotate transition for the author text 
		RotateTransition rot = new RotateTransition(Duration.millis(1000), authorText);
		rot.setByAngle(360);
		rot.setCycleCount(1);
		
		//Create a parallel transition (play all transitions at once)
		ParallelTransition welcomeParallel = new ParallelTransition();
		welcomeParallel.getChildren().addAll(scale, fade, authorFade, rot);
		welcomeParallel.setCycleCount(1);
		welcomeParallel.play();
		
		// place the text in a vbox  
		VBox welcomeBox = new VBox();
		welcomeBox.setAlignment(Pos.CENTER);
		welcomeBox.getChildren().addAll(welcomeText, authorText);
		
		// Add the vbox to the center of the borderpane
		this.setCenter(welcomeBox);
		
		//When the user clicks anywhere in the scene, invoke the lambda function
		this.setOnMouseClicked(e -> {
			//TODO: change to new scene
			//MainGame.mainStage.setScene();
		});
		
	}
}









package mp3;

import java.io.File;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class MyMusicPlayer extends Application {

	MediaPlayer player = null; //Container for the media object
	static Media media = null; //Actual mp3 file
	ImageView play;
	
	
	public static void main(String[] args) {
		Application.launch();

	}


	@Override
	public void start(Stage primaryStage) throws Exception {
		//Create a file object pointing at the Music directory
		File file = new File("Music"); //            /Music/*
		
		//check if directory actually exists?
		//if it does not, exit the application
		
		
		File[] list = file.listFiles(); //file objects
		
		
		ArrayList<String> fileNames = new ArrayList<String>();
		
		for(File f : list) { //For each file in the list
			if (f.isFile()) { //if it is a file
				fileNames.add(f.getName()); //get the name and store it in the ArrayList
			}
		}
		
		ObservableList<String> songs = (ObservableList<String>) FXCollections.observableArrayList(fileNames);
		
		BorderPane root = new BorderPane();
		ListView listOfSongs = new ListView();
		//populate the listview with the songs
		listOfSongs.setItems(songs);
		listOfSongs.setPrefWidth(300);
		listOfSongs.setMaxHeight(200);
		
		root.setLeft(listOfSongs);
		
		HBox controls = new HBox();
		controls.setAlignment(Pos.CENTER);
		play = new ImageView(new Image("play.png"));
		play.setFitHeight(100);
		play.setFitWidth(100);
		
		ImageView stop = new ImageView(new Image("stop.png"));
		stop.setFitHeight(100);
		stop.setFitWidth(100);
		
		controls.getChildren().addAll(play, stop);
		controls.setSpacing(10);
		root.setCenter(controls);
		
		play.setOnMouseClicked(e -> {
			//Code to play the file
			//if not file is selected; return
			if(listOfSongs.getSelectionModel().isEmpty()) return;
			
			//if there is no media player
			if(player == null) {
				//create a new media object
				media = new Media(
						new File("Music/" + listOfSongs.getSelectionModel().getSelectedItem().toString()
						); //full path to the mp3
				//create a new mediaPlayer object
				//play the media through the mediaPlayer
			//
			}
			
		});
		
		stop.setOnMouseClicked(e -> {
			//Code to stop the file
		});
		
		Scene scene = new Scene(root, 750, 200);
		primaryStage.setScene(scene);
		primaryStage.setTitle("My Music Player");
		primaryStage.setResizable(false);
		primaryStage.show();
	}

}

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Button;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class RectangleProgram extends Application {
	@Override
	public void start(Stage primaryStage) {
		Rectangle rectangle = new Rectangle();
		rectangle.setWidth(90);
		rectangle.setHeight(40);
		
		Button buttonRotate = new Button("Rotate");
		
		buttonRotate.setOnAction(e -> rectangle.setRotate(rectangle.getRotate() + 15));
		
		BorderPane pane = new BorderPane();
		pane.setCenter(rectangle);
		pane.setBottom(buttonRotate);
		BorderPane.setAlignment(rectangle, Pos.CENTER);
		BorderPane.setAlignment(buttonRotate, Pos.CENTER);
		
		Scene scene = new Scene(pane);
		primaryStage.setTitle("Rectangle Program");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}

import java.util.Random;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class FourCards extends Application {
	String card1 = "1.png";
	String card2 = "1.png";
	String card3 = "1.png";
	String card4 = "1.png";
	
	private Button btRefresh = new Button("Refresh");
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	@Override //Overrides the start method in the Application class
	public void start(Stage primaryStage) throws Exception {
		//Create a pane to hold the image views
		Pane pane = new HBox(10);
		pane.setPadding(new Insets(5, 5, 5, 5));
		
		Image image = new Image(card1);
		pane.getChildren().add(new ImageView(image));
		
		Image image1 = new Image(card2);
		pane.getChildren().add(new ImageView(image1));
		
		Image image2 = new Image(card3);
		pane.getChildren().add(new ImageView(image2));
		
		Image image3 = new Image(card4);
		pane.getChildren().add(new ImageView(image3));
		
		pane.getChildren().add(btRefresh);
		
		btRefresh.setOnAction(e -> refresh(pane));
		
		//Create a scene and place it in the stage
		Scene scene = new Scene(pane);
		primaryStage.setTitle("ShowImage");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void refresh(Pane pane){
		
		Random r = new Random();
		
		pane.getChildren().clear();
		
		int firstCard = r.nextInt(52)+1;
		int secondCard = r.nextInt(52)+1;
		int thirdCard = r.nextInt(52)+1;
		int fourthCard = r.nextInt(52)+1;
		
		switch(firstCard) {
		case 1:
			card1="1.png";
			break;
		case 2:
			card1="2.png";
			break;
		case 3:
			card1="3.png";
			break;
		case 4:
			card1="4.png";
			break;
		case 5:
			card1="5.png";
			break;
		case 6:
			card1="6.png";
			break;
		case 7: 
			card1="7.png";
			break;
		case 8:
			card1="8.png";
			break;
		case 9:
			card1="9.png";
			break;
		case 10:
			card1="10.png";
			break;
		case 11:
			card1="11.png";
			break;
		case 12:
			card1="12.png";
			break;
		case 13:
			card1="13.png";
			break;
		case 14:
			card1="14.png";
			break;
		case 15:
			card1="15.png";
			break;
		case 16:
			card1="16.png";
			break;
		case 17: 
			card1="17.png";
			break;
		case 18:
			card1="18.png";
			break;
		case 19:
			card1="19.png";
			break;
		case 20:
			card1="20.png";
			break;
		case 21:
			card1="21.png";
			break;
		case 22:
			card1="22.png";
			break;
		case 23:
			card1="23.png";
			break;
		case 24:
			card1="24.png";
			break;
		case 25:
			card1="25.png";
			break;
		case 26:
			card1="26.png";
			break;
		case 27: 
			card1="27.png";
			break;
		case 28:
			card1="28.png";
			break;
		case 29:
			card1="29.png";
			break;
		case 30:
			card1="30.png";
			break;
		case 31:
			card1="31.png";
			break;
		case 32:
			card1="32.png";
			break;
		case 33:
			card1="33.png";
			break;
		case 34:
			card1="34.png";
			break;
		case 35:
			card1="35.png";
			break;
		case 36:
			card1="36.png";
			break;
		case 37: 
			card1="37.png";
			break;
		case 38:
			card1="38.png";
			break;
		case 39:
			card1="39.png";
			break;
		case 40:
			card1="40.png";
			break;
		case 41:
			card1="41.png";
			break;
		case 42:
			card1="42.png";
			break;
		case 43:
			card1="43.png";
			break;
		case 44:
			card1="44.png";
			break;
		case 45:
			card1="45.png";
			break;
		case 46:
			card1="46.png";
			break;
		case 47: 
			card1="47.png";
			break;
		case 48:
			card1="48.png";
			break;
		case 49:
			card1="49.png";
			break;
		case 50:
			card1="50.png";
			break;
		case 51:
			card1="51.png";
			break;
		case 52:
			card1="52.png";
			break;
		}
		
		switch(secondCard) {
		case 1:
			card2="1.png";
			break;
		case 2:
			card2="2.png";
			break;
		case 3:
			card2="3.png";
			break;
		case 4:
			card2="4.png";
			break;
		case 5:
			card2="5.png";
			break;
		case 6:
			card2="6.png";
			break;
		case 7: 
			card2="7.png";
			break;
		case 8:
			card2="8.png";
			break;
		case 9:
			card2="9.png";
			break;
		case 10:
			card2="10.png";
			break;
		case 11:
			card2="11.png";
			break;
		case 12:
			card2="12.png";
			break;
		case 13:
			card2="13.png";
			break;
		case 14:
			card2="14.png";
			break;
		case 15:
			card2="15.png";
			break;
		case 16:
			card2="16.png";
			break;
		case 17: 
			card2="17.png";
			break;
		case 18:
			card2="18.png";
			break;
		case 19:
			card2="19.png";
			break;
		case 20:
			card2="20.png";
			break;
		case 21:
			card2="21.png";
			break;
		case 22:
			card2="22.png";
			break;
		case 23:
			card2="23.png";
			break;
		case 24:
			card2="24.png";
			break;
		case 25:
			card2="25.png";
			break;
		case 26:
			card2="26.png";
			break;
		case 27: 
			card2="27.png";
			break;
		case 28:
			card2="28.png";
			break;
		case 29: 
			card2="29.png";
			break;
		case 30:
			card2="30.png";
			break;
		case 31:
			card2="31.png";
			break;
		case 32:
			card2="32.png";
			break;
		case 33:
			card2="33.png";
			break;
		case 34:
			card2="34.png";
			break;
		case 35:
			card2="35.png";
			break;
		case 36:
			card2="36.png";
			break;
		case 37: 
			card2="37.png";
			break;
		case 38:
			card2="38.png";
			break;
		case 39:
			card2="39.png";
			break;
		case 40:
			card2="40.png";
			break;
		case 41:
			card2="41.png";
			break;
		case 42:
			card2="42.png";
			break;
		case 43:
			card2="43.png";
			break;
		case 44:
			card2="44.png";
			break;
		case 45:
			card2="45.png";
			break;
		case 46:
			card2="46.png";
			break;
		case 47: 
			card2="47.png";
			break;
		case 48:
			card2="48.png";
			break;
		case 49:
			card2="49.png";
			break;
		case 50:
			card2="50.png";
			break;
		case 51:
			card2="51.png";
			break;
		case 52:
			card2="52.png";
			break;
		}
		
		switch(thirdCard) {
		case 1:
			card3="1.png";
			break;
		case 2:
			card3="2.png";
			break;
		case 3:
			card3="3.png";
			break;
		case 4:
			card3="4.png";
			break;
		case 5:
			card3="5.png";
			break;
		case 6:
			card3="6.png";
			break;
		case 7: 
			card3="7.png";
			break;
		case 8:
			card3="8.png";
			break;
		case 9:
			card3="9.png";
			break;
		case 10:
			card3="10.png";
			break;
		case 11:
			card3="11.png";
			break;
		case 12:
			card3="12.png";
			break;
		case 13:
			card3="13.png";
			break;
		case 14:
			card3="14.png";
			break;
		case 15:
			card3="15.png";
			break;
		case 16:
			card3="16.png";
			break;
		case 17: 
			card3="17.png";
			break;
		case 18:
			card3="18.png";
			break;
		case 19:
			card3="19.png";
			break;
		case 20:
			card3="20.png";
			break;
		case 21:
			card3="21.png";
			break;
		case 22:
			card3="22.png";
			break;
		case 23:
			card3="23.png";
			break;
		case 24:
			card3="24.png";
			break;
		case 25:
			card3="25.png";
			break;
		case 26:
			card3="26.png";
			break;
		case 27: 
			card3="27.png";
			break;
		case 28:
			card3="28.png";
			break;
		case 29:
			card3="29.png";
			break;
		case 30:
			card3="30.png";
			break;
		case 31:
			card3="31.png";
			break;
		case 32:
			card3="32.png";
			break;
		case 33:
			card3="33.png";
			break;
		case 34:
			card3="34.png";
			break;
		case 35:
			card3="35.png";
			break;
		case 36:
			card3="36.png";
			break;
		case 37: 
			card3="37.png";
			break;
		case 38:
			card3="38.png";
			break;
		case 39:
			card3="39.png";
			break;
		case 40:
			card3="40.png";
			break;
		case 41:
			card3="41.png";
			break;
		case 42:
			card3="42.png";
			break;
		case 43:
			card3="43.png";
			break;
		case 44:
			card3="44.png";
			break;
		case 45:
			card3="45.png";
			break;
		case 46:
			card3="46.png";
			break;
		case 47: 
			card3="47.png";
			break;
		case 48:
			card3="48.png";
			break;
		case 49:
			card3="49.png";
			break;
		case 50:
			card3="50.png";
			break;
		case 51:
			card3="51.png";
			break;
		case 52:
			card3="52.png";
			break;
		}
		
		switch(fourthCard) {
		case 1:
			card4="1.png";
			break;
		case 2:
			card4="2.png";
			break;
		case 3:
			card4="3.png";
			break;
		case 4:
			card4="4.png";
			break;
		case 5:
			card4="5.png";
			break;
		case 6:
			card4="6.png";
			break;
		case 7: 
			card4="7.png";
			break;
		case 8:
			card4="8.png";
			break;
		case 9:
			card4="9.png";
			break;
		case 10:
			card4="10.png";
			break;
		case 11:
			card4="11.png";
			break;
		case 12:
			card4="12.png";
			break;
		case 13:
			card4="13.png";
			break;
		case 14:
			card4="14.png";
			break;
		case 15:
			card4="15.png";
			break;
		case 16:
			card4="16.png";
			break;
		case 17: 
			card4="17.png";
			break;
		case 18:
			card4="18.png";
			break;
		case 19:
			card4="19.png";
			break;
		case 20:
			card4="20.png";
			break;
		case 21:
			card4="21.png";
			break;
		case 22:
			card4="22.png";
			break;
		case 23:
			card4="23.png";
			break;
		case 24:
			card4="24.png";
			break;
		case 25:
			card4="25.png";
			break;
		case 26:
			card4="26.png";
			break;
		case 27: 
			card4="27.png";
			break;
		case 28:
			card4="28.png";
			break;
		case 29:
			card4="29.png";
			break;
		case 30:
			card4="30.png";
			break;
		case 31:
			card4="31.png";
			break;
		case 32:
			card4="32.png";
			break;
		case 33:
			card4="33.png";
			break;
		case 34:
			card4="34.png";
			break;
		case 35:
			card4="35.png";
			break;
		case 36:
			card4="36.png";
			break;
		case 37: 
			card4="37.png";
			break;
		case 38:
			card4="38.png";
			break;
		case 39:
			card4="39.png";
			break;
		case 40:
			card4="40.png";
			break;
		case 41:
			card4="41.png";
			break;
		case 42:
			card4="42.png";
			break;
		case 43:
			card4="43.png";
			break;
		case 44:
			card4="44.png";
			break;
		case 45:
			card4="45.png";
			break;
		case 46:
			card4="46.png";
			break;
		case 47: 
			card4="47.png";
			break;
		case 48:
			card4="48.png";
			break;
		case 49:
			card4="49.png";
			break;
		case 50:
			card4="50.png";
			break;
		case 51:
			card4="51.png";
			break;
		case 52:
			card4="52.png";
			break;
		}
		Image image = new Image(card1);
		pane.getChildren().add(new ImageView(image));
		
		Image image1 = new Image(card2);
		pane.getChildren().add(new ImageView(image1));
		
		Image image2 = new Image(card3);
		pane.getChildren().add(new ImageView(image2));
		
		Image image3 = new Image(card4);
		pane.getChildren().add(new ImageView(image3));
		
		pane.getChildren().add(btRefresh);
		btRefresh.setOnAction(e -> refresh(pane));
	}
}
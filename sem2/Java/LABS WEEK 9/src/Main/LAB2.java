package Main;


import Scene.LAB2SCENE;
import javafx.application.Application;
import javafx.stage.Stage;

public class LAB2 extends Application {
	public static Stage mainStage;
	
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Make the primaryStage global so any class can change scenes
		mainStage = primaryStage;
		mainStage.setScene(new LAB2SCENE());
		mainStage.setTitle("eduSource");
		mainStage.show();
	}

}

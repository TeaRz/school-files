import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Lab1 extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Text name = new Text("eduSource Game");
		
		Font font = Font.font("sans", FontWeight.BOLD, FontPosture.REGULAR, 50);
		
		name.setFont(font);
		name.setFill(Color.BURLYWOOD);
		name.setStroke(Color.BROWN);
		
		//Fade 1
		FadeTransition fade = new FadeTransition(Duration.millis(1000), name);
		fade.setFromValue(1);
		fade.setToValue(.5);
		fade.setCycleCount(4);
		fade.setAutoReverse(true);
		
		//Transition 2
		ScaleTransition scale = new ScaleTransition(Duration.millis(2000), name);
		scale.setByX(.25f);
		scale.setByY(.25f);
		
				
		//ParallelTransition
		ParallelTransition parallel = new ParallelTransition();
		parallel.getChildren().addAll(fade, scale);
		parallel.setCycleCount(1);
		parallel.play();
		
		
		StackPane pane = new StackPane();
		pane.getChildren().add(name);
		Scene scene = new Scene(pane, 550, 550);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}

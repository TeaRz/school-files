package Pane;

import javafx.animation.PathTransition;
import javafx.animation.TranslateTransition;
import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

public class LAB2PANE extends BorderPane {
	
	public int X;
	public int Y;
	
	public LAB2PANE() {
		ImageView img1 = new ImageView("image.jpg");
		img1.setFitHeight(40);
		img1.setFitWidth(40);
				
		TranslateTransition image = new TranslateTransition(Duration.millis(2000), img1);
	     image.setByX(X);
	     image.setByY(Y);
	     image.setCycleCount(PathTransition.INDEFINITE);
		
		// place the text in a vbox  
		VBox welcomeBox = new VBox();
		welcomeBox.setAlignment(Pos.CENTER);
		welcomeBox.getChildren().addAll(img1);
		
		// Add the vbox to the center of the borderpane
		this.setCenter(welcomeBox);
		
		//When the user clicks anywhere in the scene, invoke the lambda function
		this.setOnMouseClicked(e -> {
			X = 10;
		});
		
	}
}


package Panes;

import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class WelcomePane extends BorderPane {

	public WelcomePane() {
		Font welcomeFont = Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 40);
		
		Text welcomeText = new Text("Steak & Berries");
		welcomeText.setFont(welcomeFont);
		
		welcomeText.setFill(Color.RED);
		welcomeText.setStroke(Color.BLACK);
	}
	
}

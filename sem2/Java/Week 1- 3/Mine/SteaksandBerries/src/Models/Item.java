package Models;
/**
 * @author Cody Pedro 
 */
public class Item {
	
	private String name;
	private int quantity;
	
	//Constructor for Item class
	public Item(String name) {
		this.name = name;
		this.quantity = 0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String toString() {
		return getName();
	}
	
	public boolean use() {
		//If there are >0 qty then subtract 1 and return true, otherwise return false
		if (getQuantity() > 0) {
			//Subtract one from the Qty
			//this.quantity -= 1;
			setQuantity(getQuantity() - 1);
			return true;
		} else {
			return false;
		}
	}
	
}

package Models;

public abstract class Food extends Item {
	
	//Create the regeneration value property
	private int regenerationValue;
	
	//Create a construct
	public Food(String name, int regenerationValue) {
		super(name);
		this.regenerationValue = regenerationValue;
	}
	
	//Create getters and setters
	public int getRegenerationValue() {
		return regenerationValue;
	}

	public void setRegenerationValue(int regenerationValue) {
		this.regenerationValue = regenerationValue;
	}
	
	//Create a cook() method
	public abstract Food cook();
	
	//Create an eat() method
	public void eat() {
		if(use()) {
			System.out.println("You ate the " + this.getName());
		}
	}

}

package cookedFood;
import Models.Food;

public class Steak extends Food {
	
	public Steak() {
		super("Steak", 10);
	}
	
	/**
	 * @return Food
	 * Method cooks food and returns Food type
	 */
	
	public Food cook() {
		//use the steak
		use();
		
		//output a message
		System.out.println("You began to cook the Steak");
		
		//wait 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//output another message
		System.out.println("The steak is burnt. Smells like a pile of molasses");
		
		//return ??
		return null;
	}
	
}

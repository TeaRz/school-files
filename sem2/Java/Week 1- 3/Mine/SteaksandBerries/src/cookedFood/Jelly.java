package cookedFood;
import Models.Food;

public class Jelly extends Food {
	public Jelly() {
		super("Jelly", 10);
	}

	@Override
	public Food cook() {
		//use the jelly
		use();
		
		//output a message
		System.out.println("You began to cook the jelly");
		
		//wait 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//output another message
		System.out.println("The jelly is burnt. Smells like a pile of molasses. The jelly is inedible");
		
		//return ??
		return null;
	}
}

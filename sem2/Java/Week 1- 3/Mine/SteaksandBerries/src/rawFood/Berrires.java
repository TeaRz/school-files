package rawFood;
import Models.Food;
import cookedFood.Jelly;

public class Berrires extends Food {

	@Override
	public Food cook() {
		//use the berries
		use();
		//output a message saying im cooking them
		System.out.println("You began to cook the berries");
		//wait for 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//output a message saying jelly is made
		System.out.println("The berriers are cooked. Hello Jelly!");
		
		//instantiate and return a new jelly object
		return new Jelly();
		
	}

	public Berrires() {
		super("Berries", 1);
	}
	
}

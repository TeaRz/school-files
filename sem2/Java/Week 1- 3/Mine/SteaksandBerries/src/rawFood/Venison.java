package rawFood;
import Models.Food;
import cookedFood.Steak;

public class Venison extends Food {
	public Venison() {
		super("Venison", 1);
	}

	@Override
	public Food cook() {
		//use the venison
		use();
		//output a message
		System.out.println("You begin to cook the Venison");
		//wait 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//output another message
		System.out.println("The steak is cooked to perfection");
		//return
		return new Steak();
		
	}
	
	
}

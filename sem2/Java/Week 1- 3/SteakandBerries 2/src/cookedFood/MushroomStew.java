package cookedFood;

import Models.Food;

	/**
	 * Mushroomstew class
	 * 
	 * @author Cody Pedro
	 * @date Jan 10/19
	 */

public class MushroomStew extends Food {
	/**
	 * Constructor for Mushroomstew class
	 */
	public MushroomStew() {
		super("Mushroomstew", 10);
	}
	
	/**
	 * @override
	 * @return Food Cook the item and return a Food type
	 */
	public Food cook() {
		//Use the MushroomStew
		use();
		
		//Output a message
		System.out.println("You began to cook the mushroomstew");
		
		//wait for 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
				e.printStackTrace();
		}
		
		//Output another message
		System.out.println("The mushroomstew is now brunt! It was already cooked you mushroom");
		
		//Return ??
		return null;
	}

}

package cookedFood;

import Models.Food;

/**
 * Steak class
 * 
 * @author Cody Pedro
 * @date Jan 9/19
 *
 */
public class Steak extends Food {
	/**
	 * Steak constructor
	 */
	public Steak() {
		super("Steak", 10);
	}

	/**
	 * @override
	 * @return Food Cook the item and return a Food type
	 */
	public Food cook() {
		use();
		System.out.println("You began to cook the steak");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("The steak is burnt");

		return null;
	}
}

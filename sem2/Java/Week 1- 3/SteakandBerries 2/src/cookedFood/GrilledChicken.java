package cookedFood;

import Models.Food;

	/**
	 * Grilled Chicken class
	 * 
	 * @author Cody Pedro
	 * @date Jan 10/19
	 */

public class GrilledChicken extends Food {
	/**
	 * Constructor for Grilled Chicken class
	 */
	public GrilledChicken() {
		super("Grilled Chicken", 10);
	}
	
	/**
	 * @override
	 * @return Food Cook the item and return a Food type
	 */
	public Food cook() {
		//Use the Grilled Chicken
		use();
		
		//Output a message
		System.out.println("You began to cook the Grilled Chicke");
		
		//wait for 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Output another message
		System.out.println("The Grilled Chicken is now burnt! " + "How could you do such a thing?");
		
		//Return ??
		return null;
	}

}

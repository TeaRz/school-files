package cookedFood;

import Models.Food;

	/**
	 * Salad class
	 * 
	 * @author Cody Pedro
	 * @date Jan 10/19
	 */

public class Salad extends Food {
	/**
	 * Constructor for Salad class
	 */
	public Salad() {
		super("Salad", 10);
	}
	
	/**
	 * @override
	 * @return Food Cook the item and return a Food type
	 */
	public Food cook() {
		//Use the Jelly
		use();
		
		//Output a message
		System.out.println("You began to cook the salad");
		
		//Wait 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//Output another message
		System.out.println("The salad is burnt! " + "Why would you do this." + " The salad is inedible");
		
		//Return ??
		return null;
	}
}

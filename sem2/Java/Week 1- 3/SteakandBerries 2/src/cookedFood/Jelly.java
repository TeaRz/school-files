package cookedFood;

import Models.Food;

/**
 * Jelly class
 * 
 * @author Cody Pedro
 * @date Jan 9/19
 *
 */
public class Jelly extends Food {
	/**
	 * Constructor for Jelly class
	 */
	public Jelly() {
		super("Jelly", 10);
	}

	/**
	 * @override
	 * @return Food Cook the item and return a Food type
	 */
	public Food cook() {
		// Use the Jelly
		use();

		// Output a message
		System.out.println("You began to cook the jelly");

		// Wait 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Output another message
		System.out.println("The jelly is burnt! " + "Smells like a pile of molasses." + " The jelly is inedible");
		// Return ??
		return null;
	}

}

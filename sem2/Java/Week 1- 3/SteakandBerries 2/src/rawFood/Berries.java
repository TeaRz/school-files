package rawFood;

import Models.Food;
import cookedFood.Jelly;

	/**
	 * Berries class
	 * 
	 * @author Cody Pedro
	 * @date Jan 9/19
	 *
	 */
public class Berries extends Food {

	/**
	 * @override
	 * @return Food Cook the item and return a Food type
	 */
	public Food cook() {
		// use the berries
		this.use();

		// output a message saying i'm cooking them
		System.out.println("You began to cook the berries");

		// wait for 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// output a message saying jelly is made
		System.out.println("The berries are cooked. Hello Jelly!");

		// instantiate and return a new Jelly object
		return new Jelly();
	}

	/**
	 * Constructor for Berries class
	 */
	public Berries() {
		super("Berries", 1);
	}
}

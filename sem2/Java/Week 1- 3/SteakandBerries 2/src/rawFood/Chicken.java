package rawFood;

import Models.Food;
import cookedFood.GrilledChicken;

	/**
	 * Chicken class
	 * 
	 * @author Cody Pedro
	 * @date Jan 10/19
	 */

public class Chicken extends Food {

	/**
	 * @override
	 * @return Food cook the item and return a food type
	 */
	
	public Food cook() {
		//use the chicken
		use();
		
		//output a message saying i'm cooking it
		System.out.println("You began to grill the chicken.");
		
		//wait for 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//output a message saying the grilled chicken is made
		System.out.println("You have made grilled chicken. Sorry chicken little.");
		
		//instantiate and return a new grilled chicken object
		return new GrilledChicken();
	}
	
	/**
	 * Constructor for Grilled Chicken class
	 */
	public Chicken() {
		super("Chicken", 1);
	}
}

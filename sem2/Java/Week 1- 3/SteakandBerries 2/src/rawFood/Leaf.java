package rawFood;

import Models.Food;
import cookedFood.Salad;

	/**
	 *  Leaf class
	 * 
	 * @author Cody Pedro
	 * @date Jan 10/19
	 */

public class Leaf extends Food {

	/**
	 * @override
	 * @return Food cook the item and return a food type
	 */
	public Food cook() {
		//use the leaf
		use();
		
		//output a message saying i'm cooking them
		System.out.println("You began to cook the leaf");
		
		//wait for 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//output a message saying salad is made
		System.out.println("You have made a salad. Yummy Yummy");
		
		//instantiate and return a new Salad object
		return new Salad();
	}
	
	/**
	 * Constructor for Salad class
	 */
	public Leaf() {
		super("Leaf", 1);
	}
}

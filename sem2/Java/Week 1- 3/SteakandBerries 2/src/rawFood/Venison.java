package rawFood;

import Models.Food;
import cookedFood.Steak;

	/**
	 * 
	 * @author Cody Pedro
	 * @date January 9/2019
	 *
	 */
public class Venison extends Food {
	/**
	 * Constructor for Venison class
	 */
	public Venison() {
		super("Venison", 1);
	}

	/**
	 * @override
	 * @return Food Cook the item and return a Food type
	 */
	public Food cook() {
		use();
		System.out.println("You began to cook the venison");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("The steak is cooked to perfection");

		return new Steak();
	}
}

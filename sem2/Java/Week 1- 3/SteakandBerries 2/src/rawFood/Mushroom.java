package rawFood;

import Models.Food;
import cookedFood.MushroomStew;

	/**
	 * Mushroom class
	 * @author Cody Pedro
	 * @date Jan 10/19
	 */

public class Mushroom extends Food {
	
	/**
	 * @override
	 * @return Food cook the item and return a food type
	 */
	public Food cook() {
		//use the leaf
		use();
		
		//output a message saying i'm cooking them
		System.out.println("You began to cook the Mushroom");
		
		//wait for 5 seconds
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//output a message saying mushroomstew is made
		System.out.println("You have made some Mushroomstew. Dont eat too much youll turn into a mushroom");
		
		//instantiate and return a new Mushroomstew object
		return new MushroomStew();
	}
	
	/**
	 * Constructor for Mushroom class
	 */
	public Mushroom() {
		super("Mushroom", 1);
	}
}

package Models;

	/**
	 * Food class
	 * 
	 * @author Cody Pedro
	 * @date Jan 9/19
	 *
	 */

public abstract class Food extends Item {
	// Create the regen value property
	private int regenerationValue;

	/**
	 * Constructor for Food class
	 * 
	 * @param name
	 * @param regenerationValue
	 */
	public Food(String name, int regenerationValue) {
		super(name);
		this.regenerationValue = regenerationValue;
	}

	/**
	 * Getter for regenrationValue
	 * 
	 * @return regenerationValue
	 */
	public int getRegenerationValue() {
		return regenerationValue;
	}

	/**
	 * Setter for regenerationValue
	 * 
	 * @param int
	 *            regenerationValue
	 */
	public void setRegenerationValue(int regenerationValue) {
		this.regenerationValue = regenerationValue;
	}

	/**
	 * @return Food Abstract method cook()
	 */
	public abstract Food cook();

	/**
	 * @override Used to eat Food items
	 */
	public void eat() {
		if (use()) {
			System.out.println("You ate the " + this.getName());
		}
	}

}

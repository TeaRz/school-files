package Models;

public abstract class Drink extends Item {
	// create property for hydration value
	private int hydrationValue;

	// create constructor
	public Drink(String name, int hydrationValue) {
		super(name);
		this.hydrationValue = hydrationValue;
	}

	// getter & setter
	public int getHydrationValue() {
		return hydrationValue;
	}

	public void setHydrationValue(int hydrationValue) {
		this.hydrationValue = hydrationValue;
	}

	// create a drink() method
	public abstract void drink();

}

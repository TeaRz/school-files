package Models;
	
	/**
	 * Item class
	 * 
	 * @author Cody Pedro
	 * @date Jan 9/19
	 */
public class Item {

	private String name;
	private int quantity;

	/**
	 * Constructor for Item class
	 * 
	 * @param name
	 */
	public Item(String name) {
		this.name = name;
		this.quantity = 0;
	}

	/**
	 * Getter for name
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter for name
	 * 
	 * @param String
	 *            name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for quantity
	 * 
	 * @return int
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Setter for quantity
	 * 
	 * @param int
	 *            quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * toString method
	 * 
	 * @return String
	 */
	public String toString() {
		return getName();
	}

	/**
	 * Consume the item if the qty is > 0
	 * 
	 * @return Boolean
	 */
	public Boolean use() {
		// If there are >0 qty then subtract 1 & return true otherwise return false
		if (getQuantity() > 0) {
			// Subtract one from the qty
			setQuantity(getQuantity() - 1);
			return true;
		} else {
			return false;
		}
	}

}

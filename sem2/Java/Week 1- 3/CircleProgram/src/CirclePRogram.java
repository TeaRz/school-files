import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class CirclePRogram extends Application {

	public static void main(String[] args) {
		Application.launch(args);

	}

	@Override
	public void start(Stage arg0) throws Exception {
		//create a new HBox
		HBox hbox = new HBox();
		
		//Set HBox spacing to 4px
		hbox.setSpacing(4); //*****spacing between nodes.
		
		//Align HBox centered relative to the BorderPane bottom
		hbox.setAlignment(Pos.CENTER);
		
		//Create the two buttons 
		Button enlarge = new Button("Enlarge");
		Button shrink = new Button("Shrink");
		
		//Create event to enlarge circle
		enlarge.setOnAction(new EnlargeHandler());
		
		//Create event to shrink circle
		shrink.setOnAction(new ShrinkHandler());
		
		//Add the two buttons to the HBox
		hbox.getChildren().addAll(enlarge, shrink);
		
		//Create a new BorderPae
		BorderPane borderpane = new BorderPane();
		
		//Add the HBox to the bottom section of the BorderPane
		borderpane.setBottom(hbox);
		
		//Add the circle to the center section of the BorderPane
		
		//Create a new scene
		Scene scene = new Scene(borderpane, 300, 300);
		
		//Attach the scene to the primaryStage
		primaryStage.setScene(scene); 
		
		//Set the title of the primaryStage
		primaryStage.setTitle("Circle Program");
		
		//Show the primaryStage
		primaryStage.show();
		
	}
	
	public class EnlargeHandle implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent arg0) {
			//Enlarge the circle
		}
		
	}
	
	class CirclePane extends StackPane {
		private Circle circle = new Circle(50);
		
		public CirclePane() {
			getChildren().add(circle);
			circle.setStroke(Color.BLACK);
			circle.setFill(Color.WHITE);
		}
		
		public void enlarge() {
			if(circle.getRadius() < 120) {
				circle.setRadius(circle.getRadius() + 2);
			}
		}
		
		public void shrink() {
			if(circle.getRadius() > 2) {
				circle.setRadius(circle.getRadius() - 2);
			}
		}
	}
	

}

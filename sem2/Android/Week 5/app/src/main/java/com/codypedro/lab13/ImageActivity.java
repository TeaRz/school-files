package com.codypedro.lab13;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import static com.codypedro.lab13.MainActivity.IMAGE;

public class ImageActivity extends AppCompatActivity {

    int img = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        Bundle extra = getIntent().getExtras();
        if(extra != null){
            img = extra.getInt(IMAGE);
        }

        ImageView image = (ImageView) findViewById(R.id.image);
        image.setImageResource(img);
    }
}

package com.codypedro.lab13;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public static final String DETAILS = "DETAILS";
    public static final String IMAGE = "IMAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button favAnimal = (Button) findViewById(R.id.favAnimal);
        Button favFood = (Button) findViewById(R.id.favFood);
        Button aboutMe = (Button) findViewById(R.id.aboutMe);

        favAnimal.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(MainActivity.this, AboutActivity.class);
                i.putExtra(DETAILS, "ELEPHANT");
                i.putExtra(IMAGE, R.drawable.elephant);
                startActivity(i);
            }
        });

        favFood.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(MainActivity.this, AboutActivity.class);
                i.putExtra(DETAILS, "RIBS");
                i.putExtra(IMAGE, R.drawable.ribs);
                startActivity(i);
            }
        });

        aboutMe.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AboutActivity.class);
                i.putExtra(DETAILS, "CODY");
                i.putExtra(IMAGE, R.drawable.cody);
                startActivity(i);
            }
        });
    }
}

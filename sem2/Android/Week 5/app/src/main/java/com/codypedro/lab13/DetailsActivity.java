package com.codypedro.lab13;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import static com.codypedro.lab13.MainActivity.DETAILS;
import static com.codypedro.lab13.MainActivity.IMAGE;

public class DetailsActivity extends AppCompatActivity {

    String details = "details";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Bundle extra = getIntent().getExtras();
        if(extra != null){
            details = extra.getString(DETAILS);
        }

        TextView title = (TextView) findViewById(R.id.title);
        title.setText(details);
    }
}

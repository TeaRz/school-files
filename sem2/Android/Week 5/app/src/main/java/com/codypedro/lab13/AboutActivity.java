package com.codypedro.lab13;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import static com.codypedro.lab13.MainActivity.DETAILS;
import static com.codypedro.lab13.MainActivity.IMAGE;

public class AboutActivity extends AppCompatActivity {

    String details = "details";
    int image = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Bundle extra = getIntent().getExtras();
        if(extra != null){
            details = extra.getString(DETAILS);
            image = extra.getInt(IMAGE);
        }

        Button imageButton = (Button) findViewById(R.id.imageButton);
        Button detailButton = (Button) findViewById(R.id.detailButton);


        imageButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(AboutActivity.this, ImageActivity.class);
                i.putExtra(IMAGE, image);
                startActivity(i);
            }
        });

        detailButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(AboutActivity.this, DetailsActivity.class);
                i.putExtra(DETAILS, details);
                startActivity(i);
            }
        });
    }
}

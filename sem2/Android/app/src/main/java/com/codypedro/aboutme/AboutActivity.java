package com.codypedro.aboutme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import static com.codypedro.aboutme.MainActivity.TITLE;
import static com.codypedro.aboutme.MainActivity.DESCRIPTION;
import static com.codypedro.aboutme.MainActivity.IMAGE;

public class AboutActivity extends AppCompatActivity {

    String title = "title";
    String description = "description";
    int image = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Bundle extra = getIntent().getExtras();
        if(extra != null) {
            title = extra.getString(TITLE);
            description = extra.getString(DESCRIPTION);
            image = extra.getInt(IMAGE);
        }

        TextView title = findViewById(R.id.title);
        ImageView image = findViewById(R.id.image);
        TextView description = findViewById(R.id.description);
    }
}

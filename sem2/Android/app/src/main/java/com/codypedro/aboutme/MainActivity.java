package com.codypedro.aboutme;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public static final String TITLE = "TITLE";
    public static final String DESCRIPTION = "DECRIPTION";
    public static final String IMAGE = "IMAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button aboutMe = (Button) findViewById(R.id.aboutMe);
        Button favFood = (Button) findViewById(R.id.favFood);
        Button favAnimal = (Button) findViewById(R.id.favAnimal);

        aboutMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AboutActivity.class);
                i.putExtra(TITLE,"About Me");
                i.putExtra(DESCRIPTION, "description");
                i.putExtra(IMAGE, R.drawable.cody);
                startActivity(i);
            }
        });

        favFood.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AboutActivity.class);
                i.putExtra(TITLE, "Favorite Food");
                i.putExtra(DESCRIPTION, "description");
                i.putExtra(IMAGE, R.drawable.ribs);
                startActivity(i);
            }
        });

        favAnimal.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AboutActivity.class);
                i.putExtra(TITLE, "Favorite Animal");
                i.putExtra(DESCRIPTION, "description");
                i.putExtra(IMAGE, R.drawable.elephant);
                startActivity(i);
            }
        });
    }
}
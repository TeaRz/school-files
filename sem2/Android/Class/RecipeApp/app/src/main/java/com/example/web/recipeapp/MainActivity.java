package com.example.web.recipeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    /**
     * The OnCreate method is the first method that is run by ANY activity
     * We do 90% of our coding in this method
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * setContentView inflates the view to the screen for the user to see the screen
         */
        setContentView(R.layout.activity_main);
        /**
         * Programming always starts here
         * Step 1)
         * Grab the buttons and link them to the code
         * Using findViewById() we can grab ANY XML element and
         * assign it to a Java Object so that we can interface with it
         */
        Button descriptionButton = findViewById(R.id.description);
        descriptionButton.setOnClickListener(new DescriptionClickEvent());

        Button ingredientsButton = findViewById(R.id.ingredients);
        /**
         * We add an OnClickListener to the ingredientsButton
         * (Anonymous Inner Class)
         */
        ingredientsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * We want to launch the Ingredients screen/activity
                 */
                Intent i = new Intent(MainActivity.this, IngredientActivity.class);
                startActivity(i);
            }
        });
    }

    /**
     * Inner Class Event Handler
     */
    public class DescriptionClickEvent implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            /**
             * We want it to launch the description screen/activity
             */
            Intent i = new Intent(MainActivity.this, DescriptionActivity.class);
            startActivity(i);
        }
    }

}

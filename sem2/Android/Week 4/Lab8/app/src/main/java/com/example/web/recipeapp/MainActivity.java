package com.example.web.recipeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button description = (Button) findViewById(R.id.description);
        Button ingredients = (Button) findViewById(R.id.ingredients);

        description.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, DescriptionActivity.class);
                startActivity(i);
            }
        });

        ingredients.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, IngredientsActivity.class);
                startActivity(i);
            }
        });

    }
}

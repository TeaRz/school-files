package com.codypedro.gameutilityactivityversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;


public class CoinActivity extends AppCompatActivity {

    Button newCoinButton;
    TextView coinTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin);

        newCoinButton = findViewById(R.id.newCoinButton);
        coinTextView = findViewById(R.id.coinTextView);

        newCoinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random r = new Random();

                if(r.nextBoolean()){
                    coinTextView.setText("Heads");
                }
                else{
                    coinTextView.setText("Tails");
                }
            }
        });
    }
}

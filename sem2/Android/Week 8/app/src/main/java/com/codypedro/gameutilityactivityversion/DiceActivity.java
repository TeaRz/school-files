package com.codypedro.gameutilityactivityversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class DiceActivity extends AppCompatActivity {

    Button newDiceButton;
    TextView diceTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dice);

        newDiceButton = findViewById(R.id.newDiceButton);
        diceTextView = findViewById(R.id.diceTextView);

        newDiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random r = new Random();
                int newDiceValue = r.nextInt(20)+1;
                diceTextView.setText(newDiceValue + "");
            }
        });
    }
}

package com.codypedro.gameutilityactivityversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class CardActivity extends AppCompatActivity {

    Button newCardButton;
    TextView cardTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        newCardButton = findViewById(R.id.newCardButton);
        cardTextView = findViewById(R.id.cardTextView);

        newCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random r = new Random();
                int newCardValue = r.nextInt(10) +1;
                cardTextView.setText(newCardValue + "");
            }
        });
    }
}

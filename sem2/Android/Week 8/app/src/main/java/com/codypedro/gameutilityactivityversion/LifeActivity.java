package com.codypedro.gameutilityactivityversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LifeActivity extends AppCompatActivity {

    Button minusButton;
    Button plusButton;
    TextView lifeTextView;
    int lifeTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life);

        minusButton = findViewById(R.id.lifeMinusButton);
        plusButton = findViewById(R.id.lifePlusButton);
        lifeTextView = findViewById(R.id.lifeTextView);

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lifeTotal = Integer.parseInt(lifeTextView.getText().toString());
                lifeTotal++;
                lifeTextView.setText(lifeTotal+ "");
            }
        });
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lifeTotal = Integer.parseInt(lifeTextView.getText().toString());
                lifeTotal--;
                lifeTextView.setText(lifeTotal+ "");
            }
        });
    }
}

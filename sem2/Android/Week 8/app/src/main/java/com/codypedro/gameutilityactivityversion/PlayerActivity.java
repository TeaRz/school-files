package com.codypedro.gameutilityactivityversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class PlayerActivity extends AppCompatActivity {

    Button newPlayerButton;
    TextView playerTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        newPlayerButton = findViewById(R.id.newPlayerButton);
        playerTextView = findViewById(R.id.playerTextView);

        newPlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random r = new Random();
                int newPlayerValue = r.nextInt(7)+1;
                playerTextView.setText(newPlayerValue + "");
            }
        });

    }
}

package com.codypedro.tipcalculator1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    EditText billAmountEditText;
    SeekBar tipAmountSeekBar;
    TextView totalCostBefore;
    TextView totalCostAfter;
    TextView tipTextView;

    public static final NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

    public double total;
    public double bill;
    public double totalTax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        billAmountEditText = findViewById(R.id.billAmount);
        tipAmountSeekBar = findViewById(R.id.tipAmountSeekBar);
        totalCostBefore = findViewById(R.id.totalCostTextView);
        totalCostAfter = findViewById(R.id.totalCostAfterTaxTextView);
        tipTextView = findViewById(R.id.tipAmount);

        tipAmountSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                bill = (progress / 100.0);
                calculateTip();
                tipTextView.setText(progress + "%");
        }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }
    protected void calculateTip(){

        String BillEditText= billAmountEditText.getText().toString();
        double finalValue = Double.parseDouble(BillEditText);

        total = (1 + bill) * finalValue;
        totalTax = total* 1.13;

        totalCostBefore.setText(currencyFormat.format(total));
        totalCostAfter.setText(currencyFormat.format(totalTax));

    }
}

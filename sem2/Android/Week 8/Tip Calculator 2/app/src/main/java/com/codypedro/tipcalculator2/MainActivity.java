package com.codypedro.tipcalculator2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    EditText billAmountEditText;
    SeekBar tipAmountSeekBar;
    TextView totalCostBefore;
    TextView totalCostAfter;
    Button buttonTotal;
    TextView tipTextView;


    public static final NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

    public double total;
    public double bill;
    public double totalTax;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        billAmountEditText = findViewById(R.id.billAmount);
        tipAmountSeekBar = findViewById(R.id.tipAmountSeekBar);
        totalCostBefore = findViewById(R.id.totalCostTextView);
        totalCostAfter = findViewById(R.id.totalCostAfterTaxTextView);
        buttonTotal = findViewById(R.id.button);
        tipTextView = findViewById(R.id.tipAmount);


        buttonTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateTip();
            }
        });

    }
    protected void calculateTip(){

        String BillEditText= billAmountEditText.getText().toString();
        double finalValue = Double.parseDouble(BillEditText);

        double tip = tipAmountSeekBar.getProgress();

        total = bill + finalValue + tip;
        totalTax = total* 1.13;
        tipTextView.setText(tipAmountSeekBar.getProgress()+"%");
        totalCostBefore.setText(currencyFormat.format(total));
        totalCostAfter.setText(currencyFormat.format(totalTax));

    }
}

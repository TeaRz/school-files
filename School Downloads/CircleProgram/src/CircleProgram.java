import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class CircleProgram extends Application {
	
	//Create a new custom pane "CirclePane"
	private CirclePane circlepane = new CirclePane();
	
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Create a new HBox
		HBox hbox = new HBox();
		
		//Set HBox spacing to 4px
		hbox.setSpacing(4);
		
		//Align HBox nodes center
		hbox.setAlignment(Pos.CENTER);
		
		//Create two buttons
		Button enlarge = new Button("Enlarge");
		Button shrink = new Button("Shrink");
		
		//create event to enlarge circle
		enlarge.setOnAction(new EnlargeHandler());
		
		//create event to shrink circle
		shrink.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				circlepane.shrink();
			}
		});
		
		shrink.setOnAction(e -> circlepane.shrink());
		
		//Add the two buttons to the HBox
		hbox.getChildren().addAll(enlarge, shrink);
		
		//create a new BorderPane
		BorderPane borderpane = new BorderPane();
		
		//Add the HBox to the bottom section of the BorderPane
		borderpane.setBottom(hbox);
		
		//Add the circle to the center section of the BorderPane
		borderpane.setCenter(circlepane);
		
		//Create a new scene
		Scene scene = new Scene(borderpane, 300, 300);
		
		//Attach the scene to the primaryStage
		primaryStage.setScene(scene);
		
		//Set the title of the primaryStage
		primaryStage.setTitle("Circle Program");
		
		//Show the primaryStage
		primaryStage.show();
		
	}
	
	private class EnlargeHandler implements EventHandler<ActionEvent> {
		@Override
		public void handle(ActionEvent event) {
			//Enlarge the circle
			circlepane.enlarge();
		}
	}
	
	//CirclePane extends StackPane which means CirclePane is created within a new StackPane
	public class CirclePane extends StackPane {
		//Create a new JavaFX circle node
		private Circle circle = new Circle(50); 
		
		public CirclePane() {
			//Add the JavaFX Circle node to the "CirclePane" aka StackPane
			getChildren().add(circle);
			circle.setStroke(Color.BLACK);
			circle.setFill(Color.WHITE);
		}
		
		public void enlarge() {
			if (circle.getRadius() < 120) {
				circle.setRadius(circle.getRadius() + 2);
			}
		}
		
		public void shrink() { 
			if (circle.getRadius() > 2) {
				circle.setRadius(circle.getRadius() - 2);
			}
		}
	}

}

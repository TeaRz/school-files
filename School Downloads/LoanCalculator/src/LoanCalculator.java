import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class LoanCalculator extends Application {
	
	//Create 5 textfields
	private TextField tfAnnualInterestRate = new TextField();
	private TextField tfNumberOfYears = new TextField();
	private TextField tfLoanAmount = new TextField();
	private TextField tfMonthlyPayment = new TextField();
	private TextField tfTotalPayment = new TextField();
	
	//Create the calculate button
	private Button btCalculate = new Button("Calculate");
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// create a gridpane and add the appropriate spacing between cells
		GridPane grid = new GridPane();
		grid.setVgap(5);
		grid.setHgap(5);
		
		// add the labels and textfields to the gridpane
		// GridePane positions: (COL, ROW)
		// +---------------+
		// | (0,0) | (1,0) |
		// |---------------|
		// | (0,1) | (1,1) |
		// |---------------|
		// | (0,2) | (1,2) |
		// +---------------+
		grid.add(new Label("Annual Interest Rate"), 0, 0);
		grid.add(tfAnnualInterestRate, 1, 0);

		grid.add(new Label("Number of Years"), 0, 1);
		grid.add(tfNumberOfYears, 1, 1);
		
		grid.add(new Label("Loan Amount"), 0, 2);
		grid.add(tfLoanAmount, 1, 2);

		grid.add(new Label("Monthly Payment"), 0, 3);
		tfMonthlyPayment.setEditable(false); //Textfield is not editable (used for output only)
		grid.add(tfMonthlyPayment, 1, 3);

		grid.add(new Label("Total Payment"), 0, 4);
		tfTotalPayment.setEditable(false); //Textfield is not editable (used for output only)
		grid.add(tfTotalPayment, 1, 4);

		// add the calculate button to the gridpane
		grid.add(btCalculate, 1, 5);

		// align them to the center
		grid.setAlignment(Pos.CENTER);
		
		// set the alignment of the text in each box to the bottom right
		tfAnnualInterestRate.setAlignment(Pos.BOTTOM_RIGHT);
		tfNumberOfYears.setAlignment(Pos.BOTTOM_RIGHT);
		tfLoanAmount.setAlignment(Pos.BOTTOM_RIGHT);
		tfMonthlyPayment.setAlignment(Pos.BOTTOM_RIGHT);
		tfTotalPayment.setAlignment(Pos.BOTTOM_RIGHT);

		// set the button to align to the bottom right
		grid.setHalignment(btCalculate, HPos.RIGHT);
		
		// create an actionevent for the button
		btCalculate.setOnAction(e -> calculatePayment());
		
		// create a scene & add the pane to the scene
		Scene scene = new Scene(grid, 400, 250);
		
		// set the title of the stage
		primaryStage.setTitle("Loan Calculator");
		
		// set the scene of the stage
		primaryStage.setScene(scene);
		
		// show the stage
		primaryStage.show();
	}

	//Event handler
	public void calculatePayment() {
		//Retrieve the inputs from the textfields and convert them to the appropriate data type
		double interest = Double.parseDouble(tfAnnualInterestRate.getText());
		int year = Integer.parseInt(tfNumberOfYears.getText());
		double loanAmount = Double.parseDouble(tfLoanAmount.getText());
		
		//Instantiate a new loan object
		Loan loan = new Loan(interest, year, loanAmount);
		
		//Format the output the of the calculation and place the results in the output textfields
		tfMonthlyPayment.setText(String.format("$%.2f", loan.getMonthlyPayment()));
		tfTotalPayment.setText(String.format("$%.2f", loan.getTotalPayment()));
	}
	
	//Inner class "Loan" used to calculate payment information
	private class Loan {
		private double annualInterestRate;
		private int numberOfYears;
		private double loanAmount;
		
		public Loan(double annualInterestRate, int numberOfYears, double loanAmount) {
			this.annualInterestRate = annualInterestRate;
			this.numberOfYears = numberOfYears;
			this.loanAmount = loanAmount;
		}
		
		public double getMonthlyPayment() {
			double monthlyInterestRate = annualInterestRate / 1200;
			double monthlyPayment = loanAmount * monthlyInterestRate / 
					(1-(1 / Math.pow(1+monthlyInterestRate, numberOfYears*12)));
			return monthlyPayment;	
		}
		
		public double getTotalPayment() {
			double totalPayment = getMonthlyPayment() * numberOfYears * 12;
			return totalPayment;
		}
	}
}

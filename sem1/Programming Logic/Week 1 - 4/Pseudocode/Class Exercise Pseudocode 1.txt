// Program Name: Patron Tracker
// Purpose: Calculate the total number of customers and average number of customers
// Created by: Cody Pedro
// Date last modifiedL Mon Sep. 10, 2018

Start
	//Declare variables
	Declare Numeric custHour1, custHour2, custHour3, custHour4 // number of customers in each hour
	Declare Numeric averageCustomers // average customers per hour
	Declare Numeric totalCustomers // total numer of customers 
	
	//Declare constants
	Declare Constant TOTAL_HOURS = 4 // total number of hour open
	
	//Welcome user
	Display "Welcome to the Android's Dungeon and Baseball Card Shop's Customer Tally Program"
	
	//Get the user info
	Display "Enter the number of customers from 12:00 - 1:00pm"
	Input custHour1
	
	Display "Enter the number of customers from 1:00 - 2:00pm"
	Input custHour2
	
	Display "Enter the number of customers from 2:00 - 3:00pm"
	Input custHour3
	
	Display "Enter the number of customers from 3:00 - 4:00pm"
	Input custHour4
	
	//Perform calculatins
	totalCustomers = custHour1 + custHour2 + custHour3 + custHour4
	
	averageCustomers = totalCustomers/TOTAL_HOURS
	
	//Display the results
	Display "The number of customers from 12:00 - 1:00pm was: " + custHour1
	Display "The number of customers from 1:00 - 2:00pm was: " + custHour2
	Display "The number of customers from 2:00 - 3:00pm was: " + custHour3
	Display "The number of customers from 3:00 - 4:00pm was: " + custHour4
	
	Display "The total number of customers for the day is: " + totalCustomers
	
	Display "The average of customers for the day is: " + averageCustomers
	
	//Thank the user
	Display "Thank you for using our program."
	
End
package Marty;
import java.util.Scanner;

import Truss.ComparisonCalculator;

public class HighestCalculaltor {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double num;
		double highest;
		double lowest;
		//Welcome the user and explain the software
		System.out.println("Welcome to the highest calculator\n"
						 + "This program will allow you to enter numbers"
						 + "and will tell you the highest number entered");
		//Allow the user to input a number
		System.out.println("Enter a number:");
		num = input.nextDouble();
		highest = num;
		lowest = num;
		//Loop until the number entered is -1
		do {
			System.out.println("Enter a number (enter -1 to quit):");
			num = input.nextDouble();
			if(num != -1) {
				highest = ComparisonCalculator.max(num, highest);
				lowest = ComparisonCalculator.min(num, lowest);
			}
			//Calculate if the number entered is the highest ever recorded
		}while(num != -1);
		//When the user is finished tell them the highest number
		System.out.println("The highest number entered was " + highest);
		System.out.println("The lowest number entered was " + lowest);

		input.close();
	}

}

package Marty;
import java.util.Scanner;

public class LoanEstimator {

	public static void main(String[] args) {
		boolean loanApproved = true;
		final double PRIME = 2;
		double loanAmount = 0;
		int creditScore = 0;
		double interest = 0;
		Scanner input = new Scanner(System.in);
		//Introduce the user to the program
		System.out.println("Welcome to the GNB loan quoting software\n"
						 + "Please enter the amount you would like to borrow");
		//Gather the inputs (Loan Amount, Credit Score)
		loanAmount = input.nextDouble();
		System.out.println("Please enter your current credit score:");
		creditScore = input.nextInt();
		//Calculate the interest rate
		//652
		if(creditScore >= 781) { //if they have an excellent credit score
			//652 >= 781? = false
			interest = PRIME + 0;
		}
		else if(creditScore >= 661) { //if they have a good credit score
			   //652 >= 661 ? = false
			if(loanAmount <= 30000) {
				//if they borrow less than or equal to 30,000
				interest = PRIME + 0.5;
			}
			else {
				//if they borrow more than 30,000
				interest = PRIME + 1;
			}
		}
		else if(creditScore >= 601) {//if they have a fair credit score
			  //652 >= 601? = true
			if(loanAmount <= 30000) {
				//if they borrow less than or equal to 30,000
				interest = PRIME + 1;
			}
			else {
				//if they borrow more than 30,000
				interest = PRIME + 2;
			}
		}
		else if(creditScore >= 500) {//if they have a poor credit score
			if(loanAmount <= 30000) {
				//if they borrow less than or equal to 30,000
				interest = PRIME + 2;
			}
			else {
				//if they borrow more than 30,000
				interest = PRIME + 5;
			}
		}
		else if(creditScore < 500) {//if they have a bad credit score
			if(loanAmount <= 30000) {
				//if they borrow less than or equal to 30,000
				interest = PRIME + 5;
			}
			else {
				System.out.println("Loan Denied");
				loanApproved = false;
			}
		}
		if(loanApproved) {
			//Output if they are approved
			//Output the amount borrowed, interest rate 
			System.out.println("Based on your credit rating we can offer you" 
							   + " a " + loanAmount + " loan at " + interest 
							   + "% interest rate" );
			//Output the amount paid over 5 years
			double totalFiveYears = (loanAmount * (interest/100) * 5) 
					+ loanAmount; 
			System.out.println("Total paid over 5 years: $" + totalFiveYears);
			//Output the amount paid over 10 years
			double totalTenYears = (loanAmount * (interest/100) * 10) 
					+ loanAmount;
			System.out.println("Total paid over 10 years: $" + totalTenYears);
		}
		input.close();
		
	}

}

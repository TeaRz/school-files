package Truss;
import java.util.Scanner;

public class ComparisonCalculator {

	//We can put functions/methods here
	
	public static void main(String[] args) {
		double firstNumber = 0;
		double secondNumber = 0;
		Scanner input = new Scanner(System.in);
		//Welcome the user to the software
		System.out.println("Welcome, this program makes the following comparisons");
		//Tell the user what the software does
		System.out.println("1) if one number is larger than another\n"
						 + "2) if one number is smaller than another\n"
						 + "3) if two numbers are the same"); 
		//Get the user to input two numbers
		System.out.println("Please enter the first number for calculation");
		firstNumber = input.nextDouble();
		System.out.println("Please enter the second number for calculation");
		secondNumber = input.nextDouble();
		
		System.out.println("The larger number is " +
		max(firstNumber, secondNumber));
		System.out.println("The smaller number is " 
		+ min(firstNumber, secondNumber));
		equals(firstNumber, secondNumber);
		//Output the messages to the user
		input.close();
	}
	
	/**
	 * This method is used to find the min number of two numbers
	 * @param num1 is the first number to compare
	 * @param num2 is the second number to compare
	 * @return the smaller of two values
	 * @author web
	 */
	public static double min(double num1, double num2) {
		if(num1 < num2) {
			return num1;
		}
		else {
			return num2;
		}
	}
	/**
	 * 
	 * @param n1
	 * @param n2
	 * @return
	 */
	public static double max(double n1, double n2) {
		if(n1 > n2) {
			return n1;
		}
		else {
			return n2;
		}
	}
	/**
	 * this will describe what this functions does
	 * @param num1
	 * @param num2
	 */
	public static void equals(double num1, double num2) {
		if(num1 == num2) {
			System.out.println("The two numbers are equal");
		}
		else {
			System.out.println("The two numbers are not equal");
		}
	}
	
	
	
}

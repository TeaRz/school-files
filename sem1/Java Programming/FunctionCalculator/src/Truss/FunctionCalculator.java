package Truss;
import java.util.Scanner;

public class FunctionCalculator {

	public static void main(String[] args) {
		//Allows for user input from the keyboard
		Scanner input = new Scanner(System.in);
		//Creating variables or memory locations of different data types
		//Variables are used to store information (buckets of different sizes)
		byte choice; // -128 -> 127
		double firstNumber; //0.324532523552 allows for decimal point
		int secondNumber; 
		double solution;
		//Show a menu to the user
		System.out.println("Welcome to the calculator app\n"
						 + "1) Calculate a number raised to the power of another"
						 + " number"
						 + "2) Calculate logarithm 10 of a number\n"
						 + "3) Calculate the square root of a number\n"
						 + "Please select a number (1-3):");
		//Allow the user to select an item in the Menu
		choice = input.nextByte();
		//Perform the appropriate action
		//Prompt for the appropriate number of inputs
		System.out.println("Please enter the first number for calculation:");
		firstNumber = input.nextDouble();
		//Switch statements allow the programmer to selectively run different lines of code
		//it does this based on the value of (in this case) choice
		switch(choice) {
			case 1: //choice == 1
				System.out.println(firstNumber + " raised to the power of: ");
				secondNumber = input.nextInt();
				solution = Math.pow(firstNumber, secondNumber);
				System.out.println(firstNumber  + " raised to the power of " +
				                   secondNumber + " evaluates to " + solution);
			break;
			case 2: //choice == 2
					solution = Math.log10(firstNumber);
					System.out.println("Log10("+firstNumber+") evaluates to " 
					+ solution);
			break;
			case 3: //choice == 3
					solution = Math.sqrt(firstNumber);
					System.out.println("The square root of " + firstNumber + " is " +
										solution);
			break;
		}
		//Perform the appropriate output
		input.close();
	}

}

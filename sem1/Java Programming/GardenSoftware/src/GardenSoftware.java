import java.util.Scanner;

public class GardenSoftware {

	public static void main(String[] args) {
			double length;
			double radius;
			int choice = 0; 
			Scanner input = new Scanner(System.in);
			//Welcome the user to the software
			System.out.println("Welcome to the garden quoting software");
			//asking the user if they want to build a circle or square garden
			do {
				System.out.println("What type of garden would you like to build?\n"
								 + "1) Circle Garden\n"
								 + "2) Square Garden");
				choice = input.nextInt();
				if((choice != 1) && (choice != 2)) {
					System.out.println(choice + " is an invalid selection");
				}
			}while((choice != 1) && (choice != 2));
			//ask for either the radius or length
			//display the garden description to the screen
			switch(choice) {
				case 1:
					CircleGarden garden = new CircleGarden();
					System.out.println("What is the radius of the garden?");
					radius = input.nextDouble();
					garden.setRadius(radius);
					System.out.println(garden);
				break;
			
				case 2:
					SquareGarden garden1 = new SquareGarden();
					System.out.println("What is the length of the garden?");
					length = input.nextInt();
					garden1.setLength(length);
					System.out.println(garden1);
				break;
			}
	
			input.close();
	}

}

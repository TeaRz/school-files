
public class CircleGarden {
		private double radius;
		
		public CircleGarden() {
			radius = 1;
		}
		
		public CircleGarden(double radius) {
			this.radius = radius;
		}

		public double getRadius() {
			return radius;
		}

		public void setRadius(double radius) {
			this.radius = radius;
		}
		
		public double getCircumference() {
			return 2 * Math.PI * radius;
		}
		
		public double getArea() {
			return Math.PI * (radius * radius);
		}
		
		public double getCost() {
			return 5 * getArea();
		}
		
		public String toString() {
			return "The circle garden with the radius of " + radius + "\n"
					+ " and the circumference of " + getCircumference() + "\n"
					+ " has an area of " + getArea() + " and will cost $"
					+ getCost();
		}
		
}

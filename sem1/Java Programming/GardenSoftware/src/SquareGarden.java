
public class SquareGarden {
	//Create local variables / member variables
	//Properties/attr
	private double length;
	private double width;
	
	//Create a constructor method
	public SquareGarden() {
		length = 1;
		width = length;
	}
	public SquareGarden(double length) {
		this.length = length;
		this.width = length;
	}
	
	//Create getters and setters

	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
		this.width = length;
	}
	
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
		this.length = width;
	}
	
	//Create additional methods
	
	public double getPerimeter() {
		return 4 * length;
	}
	
	public double getArea() {
		return length * width;
	}
	
	public double getCost() {
		return 5 * getArea();
	}
	//Create a toString method
	public String toString() {
		return  "The garden with the length of " + length + "\n"
				+ " and the width of " + width + " has an area of " + getArea()
				+"\n and will cost $" + getCost();
	}
}

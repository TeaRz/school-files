import java.util.Random;

public class HeadsORTails {

	public static void main(String[] args) {
		Random r = new Random();
		
		int count = 0;
		int coin = 0;
		int heads = 0;
		int tails = 0;
		String coinFlip = "";
		
		while(count < 1000000) {
			count++;
			
			coin = r.nextInt(2)+1;
			
			switch(coin) {
			case 1:
				coinFlip = "Heads";
				heads++;
				break;
			case 2:
				coinFlip = "Tails";
				tails++;
				break;
			}
			
			}
		System.out.println(heads + " Heads. " + tails + " Tails.");

	}

}

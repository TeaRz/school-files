import java.util.Random;

public class PickACard {
	
	public static void main(String[] args) {
		
		Random r = new Random();
		
		int suit = 0;
		int card = 0;
		String suitName = "";
		String cardName = "";
		
		
		suit = r.nextInt(13)+1;
		card = r.nextInt(3)+1;
		
		switch(suit) {
		case 1:
			suitName = "Ace";
			break;
		case 2:
			suitName = "1";
			break;
		case 3:
			suitName = "2";
			break;
		case 4:
			suitName = "3";
			break;
		case 5:
			suitName = "4";
			break;
		case 6:
			suitName = "5";
			break;
		case 7:
			suitName = "6";
			break;
		case 8:
			suitName = "7";
			break;
		case 9:
			suitName = "8";
			break;
		case 10:
			suitName = "9";
			break;
		case 11:
			suitName = "10";
			break;
		case 12:
			suitName = "Jack";
			break;
		case 13:
			suitName = "Queen";
			break;
		case 14:
			suitName = "King";
			break;
		}
		
		switch(card) {
		case 1:
			cardName = "Clubs";
			break;
		case 2:
			cardName = "Diamonds";
			break;
		case 3:
			cardName = "Hearts";
			break;
		case 4:
			cardName = "Spades";
			break;
		}
		
		System.out.println("The card you picked is " + suitName + " of " + cardName);

	}
	
}

import java.util.Random;
import java.util.Scanner;

public class Add3Numbers{
	
	public static void main(String[] args) {
    Random rand = new Random();

        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Welcome to the adding program");
        
        int a = rand.nextInt(9);
        int b = rand.nextInt(9);
        int c = rand.nextInt(9);
        
        System.out.println("" + a + " + " + b + " + " + c);
        
        int answer = scanner.nextInt();
        
        if(answer == (a+b+c)) {
            System.out.println("You got it right");
        }else {
        	System.out.println("You got it wrong");
        }
        
        scanner.close();
	}
}
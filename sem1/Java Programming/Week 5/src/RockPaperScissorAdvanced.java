import java.util.Random;
import java.util.Scanner;

public class RockPaperScissorAdvanced {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Random r = new Random();
		
		int choice = 0;
		int randomNumber = 10;
		int win = 0;
		int lose = 0;
		int again = 1;
		System.out.println("Welcome to Rock Paper Scissors");
	do {		
			while(win != 2 && lose != 2) {
				System.out.println("-------------------- \nRock (0) | Paper (1) | Scissors (2)");
				choice = input.nextInt();
				randomNumber = r.nextInt(2);
				
				switch(choice) {
				case 0:
					System.out.println("You picked Rock");
				break;
				case 1:
					System.out.println("You picked Paper");
				break;
				case 2:
					System.out.println("You picked Scissors");
				break;
				}
				
				switch(randomNumber) {
				case 0:
					System.out.println("The computer picked ROCK");
					if(choice == 0){
							System.out.println("Its a Draw");
						} else if(choice == 1) {
							System.out.println("You Win!");
							win++;
						} else if(choice == 2) {
							System.out.println("You Lose");
							lose++;
						}
					break;
				case 1:
					System.out.println("The computer picked PAPER");
					if(choice == 0){
						System.out.println("You Lose");
						lose++;
					} else if(choice == 1) {
						System.out.println("Its a Draw");
					} else if(choice == 2) {
						System.out.println("You Win!");
						win++;
					}
					break;
				case 2:
					System.out.println("The computer picked SCISSORS");
					if(choice == 0){
						System.out.println("You Win!");
						win++;
					} else if(choice == 1) {
						System.out.println("You Lose");
						lose++;
					} else if(choice == 2) {
						System.out.println("Its a Draw");
					}
					break;
				}
				
					break;
			}
			System.out.println(win + " wins. " + lose + " lose.");
	}while (win != 2 && lose != 2);
		
		System.out.println("The program has finished. Would you like to play again? \n 1) Yes \n 2) No");
		again = input.nextInt();
		
		if(again == 1) {
			main(args);		
		}
		
		input.close();
	}

}

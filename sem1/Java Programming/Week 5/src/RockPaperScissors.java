import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Random r = new Random();
		
		int choice = 0;
		int randomNumber = 10;
		int again = 1;
		System.out.println("Welcome to Rock Paper Scissors");
		do {
			
			do {
				System.out.println("-------------------- \nRock (0) | Paper (1) | Scissors (2)");
				choice = input.nextInt();
				randomNumber = r.nextInt(2);
				
				switch(choice) {
				case 0:
					System.out.println("You picked Rock");
				break;
				case 1:
					System.out.println("You picked Paper");
				break;
				case 2:
					System.out.println("You picked Scissors");
				break;
				}
				
				switch(randomNumber) {
				case 0:
					System.out.println("The computer picked ROCK");
					if(choice == 0){
							System.out.println("Its a Draw");
						} else if(choice == 1) {
							System.out.println("You Win!");
						} else if(choice == 2) {
							System.out.println("You Lose");
						}
					break;
				case 1:
					System.out.println("The computer picked PAPER");
					if(choice == 0){
						System.out.println("You Lose");
					} else if(choice == 1) {
						System.out.println("Its a Draw");
					} else if(choice == 2) {
						System.out.println("You Win!");
					}
					break;
				case 2:
					System.out.println("The computer picked SCISSORS");
					if(choice == 0){
						System.out.println("You Win!");
					} else if(choice == 1) {
						System.out.println("You Lose");
					} else if(choice == 2) {
						System.out.println("Its a Draw");
					}
					break;
				}
				
					break;
			} while(choice == randomNumber);
			
			System.out.println("Would you like to play agian? \nYes (1) | No (0)");
			again = input.nextInt();
		} while(again == 1);
		
		input.close();

	}

}

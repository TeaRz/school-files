import java.util.Scanner;
import java.util.Random;

public class RockPaperScissorAdvancedTEST {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Random r = new Random();
		
		int choice = 0;
		int win = 0;
		int lose = 0;
		int again = 0;
		
		System.out.println("Welcome to Rock Paper Scissors Advanced");
		do {
			
			while(win != 2 && lose != 2) {
				System.out.println("Please enter 1 for win and 2 for lose");
				choice = input.nextInt();
				if(choice == 1) {
					win++;
				} else {
					lose++;
				}
				System.out.println("You have won " + win + " times. You have lost " + lose + " times.");
			}
			
		} while(again == 1);
		
		input.close();
	}

}

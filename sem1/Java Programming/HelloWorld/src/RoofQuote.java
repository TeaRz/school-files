
public class RoofQuote {

	public static void main(String[] args) {
		//Get the cost per square foot
		double shingleCost = 3.99;
		//Get the roofs square feet
		double customerRoof = 892.22;
		//Get the installation cost per square foot
		double installationCost= 2.99;
		//Calculate and output the total
		double total = shingleCost * customerRoof * installationCost;
		
		System.out.printf("The cost is %.2f * %.2f * %.2f is %.2f", shingleCost, customerRoof, installationCost, total);
		
	}

}

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListsSum {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<Double> list = new ArrayList<>();
		
		System.out.println("Please enter 5 numbers");
		
		for(int i = 0; i < 5; i++) {
			list.add(i, input.nextDouble());
		}
		
		System.out.println("The sum of all the numbers you input is: " + sum(list));
		input.close();
	}
	
	public static Double sum(ArrayList<Double> list) {
		double sum = 0;
		
		sum = list.get(0) + list.get(1) + list.get(2) + list.get(3) + list.get(4);
		
		return sum;
	}
}

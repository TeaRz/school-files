import java.util.Arrays;
import java.util.Scanner;

public class ClassListGenerator {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int numStudents;
		
		//Welcome the user to the software
		System.out.println("Welcome to the Class List Software");
		
		//Ask the user to enter the number of students in the class
		System.out.println("Please enter the number of students in the class:");
		numStudents = input.nextInt();
		//Escape the enter key
		input.nextLine();
		//Create an array to store all the names of the students
		String[] studentNames = new String[numStudents];
		
		//Create a loop that will populate the array
		System.out.println("Please enter the name of each student followed by the neter key");
		for(int i = 0; i < studentNames.length; i++) {
			studentNames[i] = input.nextLine();
		}
		
		//Ask the user alphabetical or reverse alphabetical order
		System.out.println("Would you like the list in alphabetical(1): or reverse alphabetical order(2):");
		int order = input.nextInt();
		printSortedList(studentNames, order);
		//sort the array
		//display the list of names
		input.close();
	}
	
	/**
	 * This method will take an array and sort the array and print the values to the screen
	 * @author Cody Pedro
	 * @param list represents the list to be sorted
	 * @param order represents alphabetical (1) or reverse alphabetical (any other number)
	 */
	public static void printSortedList(String[] list, int order) {
		Arrays.sort(list);
		if(order == 1) {
			for(String listItem: list) {
				System.out.println(listItem);
			}
		}
		else {
			//if we have a list of size 6
			//the index locations are [0] - [5]
			//If i want to access the last location in the list
			//I use list.length-1 (6-1) = 5
			//this for loop starts at position [5] and goes down to [0] 
			for(int i = (list.length-1); i>= 0; i--) {
				System.out.println(list[i]);
			}
		}
	}
}

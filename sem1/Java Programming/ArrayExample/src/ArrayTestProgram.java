import java.util.Scanner;

public class ArrayTestProgram {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//When creating an array we have already established the following syntax
		int[] marks = new int[4];
		//This will create an array with locations marks[0], marks[1], 
			//marks[2], marks[3]
		//All of which are like individual variables that can each store a value
		
		//We can use the following syntax to populate each location
		marks[0] = 99;
		marks[1] = 88;
		marks[2] = 77;
		marks[3] = 66;
		
		//Just like a variable we can access those values at any time
		System.out.println(marks[0]);
		
		//We can access the size of an array by using the following
		System.out.println("The marks array currently contains " + marks.length + " items");
		//notice how when we created the array we used the size 4 (which matches the output
		
		//We can also build arrays where we do not define the size
		String[] words = {"Good", "Better", "Best", "never", "let", "it", "rest", "until", "your", "good", "is", " better", "than", "your", "betters", "best"};
		//Although we did not define the size a fixed array was still created
		System.out.println("The words array contains " + words.length + " items");
		//Just like the marks array we can access any of the values in the array output the first and last value
		System.out.println(words[0] + " " + words[15]);
		//Lets take a look at how we can output all the items in an array
		//words[0] = "Good"
		//words[1] = "Better"
		//words[2] = "Best"
		//words[3] = "never"
		//words[4] = "let"
		//words[5] = "it"
		//words[6] = "rest"
		//words[7] = "until"
		//words[8] = "your"
		//words[9] = "good"
		//words[10] = "is"
		//words[11] = "better"
		//words[12] = "than"
		//words[13] = "your"
		//words[14] = "betters"
		//words[15] = "best"
		for(int i = 0; i <words.length; i++) {
			//i = 0 -> 15
			System.out.print(words[i] + " ");
		}
		System.out.println();
		
		//Create another example where we create and populate the array all based on user input
		int numGrades;
		System.out.println("How many grades are you about to enter?");
		numGrades = input.nextInt();
		
		//Create an array large enough to store all the user input
		int[] grades = new int[numGrades];
		
		for(int i = 0; i < grades.length; i++) {
			System.out.println("Enter the value for grades [" + i + "]:");
			grades[i] = input.nextInt();
		}
		for(int i = 0; i < grades.length; i++) {
			System.out.print(grades[i] + " ");
		}
		
		//for each loop
		for(int grade: grades) {
			System.out.print(grade + " ");
		}
		for(String word: words) {
			System.out.print(word + " ");
		}
		
		String word = "Charger";
		//"c, "h", "a", "r", "g", "e", "r"
		//[0], [1], [2], [3], [4], [5], [6]
		char[] brokenUpWord = word.toCharArray();
		
		input.close();
	}

}

import java.util.Scanner;

public class FanProgram {

	public static void main(String[] args) {
		//Declare constants and variables
		final int SLOW = 1;
		final int MEDIUM = 2;
		final int FAST = 3;
		
		boolean on;
		String color;
		int speed;
		double radius;
		
		//Creates Scanner and two Fans
		Scanner input =  new Scanner(System.in);
		Fan fan1 = new Fan();
		Fan fan2 = new Fan();
		
		//Welcomes the user and tells the user to input information
		System.out.println("Input \nFan 1 \nSpeed: MAXIUM Radius: 10 Color: YELLOW On: True" + 
							"\nFan 2 \nSpeed: MEDIUM Radius: 5 Color: Blue On: False");
		
		//Asks user for fan 1
		speed = input.nextInt();
		if(speed == 1) {
			speed = SLOW;
		} else if(speed == 2) {
			speed = MEDIUM;
		} else if(speed == 3) {
			speed = FAST;
		}
		fan1.setSpeed(speed);
		radius = input.nextDouble();
		fan1.setRadius(radius);
		color = input.next();
		fan1.setColor(color);
		on = input.nextBoolean();
		fan1.setOn(on);
		
		//Asks user for fan 2
		speed = input.nextInt();
		if(speed == 1) {
			speed = SLOW;
		} else if(speed == 2) {
			speed = MEDIUM;
		} else if(speed == 3) {
			speed = FAST;
		}
		fan2.setSpeed(speed);
		radius = input.nextDouble();
		fan2.setRadius(radius);
		color = input.next();
		fan2.setColor(color);
		on = input.nextBoolean();
		fan2.setOn(on);
		
		//Prints out both fan 1 and fan 2
		System.out.println("===============");
		System.out.println(fan1.toString());
		System.out.println("===============");
		System.out.println(fan2.toString());
		
		input.close();
	}

}


public class Fan {
	private int speed = 1;
	private Boolean on = false;
	private double radius = 5;
	private String color = " blue";
	
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public Boolean getOn() {
		return on;
	}
	public void setOn(Boolean on) {
		this.on = on;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public String toString() {
		return "The fan is on: " + on + 
				"\n" + "The fan is: " + color + 
				"\n" + "The speed of the fan is: " + speed + 
				"\n" + "The radius of the fan is: " + radius;
	}
}

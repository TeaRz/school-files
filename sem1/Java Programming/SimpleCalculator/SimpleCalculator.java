import java.util.Scanner;

public class SimpleCalculator {
	
	public static void main(String[]args) { 
		Scanner input = new Scanner(System.in);
		int choice;
		double firstNumber;
		double secondNumber;
		double solution;
		
		solution = 0;
		
		//Display all possible forms of arithmetic
		System.out.println("Welcome to the Calculator app\n"
				+ "1) Perform Multiplication\n"
				+ "2) Perform Division\n"
				+ "3) Perform Subtraction\n"
				+ "4) Perform Addition\n"
				+ "5) Perform Modulus\n"
				+ "Please select a number (1-5):");
		//Allow the user to input which form of arithmetic they would like to do
		choice = input.nextInt();
		//Allow the user to input two numbers
		System.out.println("Please enter the first number");
		firstNumber = input.nextDouble();
		
		System.out.println("Please enter the second number");
		secondNumber = input.nextDouble();
		//Perform the selected arithmetic on the inputed numbers
		
		//We want to run on the following based on the value that is in choice
		// 1) multiplication 2) division ... etc
		switch (choice) { //1 - 5 (4)
		case 1:
			// choice 1
			solution = firstNumber * secondNumber;
		break;
		case 2:
		// choice 2
			solution = firstNumber / secondNumber;
		break;
		case 3:
			// choice 3
			solution = firstNumber - secondNumber;
			break;
		case 4:
			// choice 4
			solution = firstNumber + secondNumber;
			break;
		case 5:
			// choice 5
			solution = firstNumber % secondNumber;
		break;
		default:
			System.out.println("You did not enter a valid number");
		break;
		}
		//Output the solution
		System.out.println("The solution is " + solution);
		//Output the solution
		input.close();
	}

}

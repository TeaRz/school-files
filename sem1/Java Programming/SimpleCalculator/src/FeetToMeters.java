import java.util.Scanner;

public class FeetToMeters {

	public static void main(String[] args) {
		double feet;
		double meters;
		
		Scanner input = new Scanner(System.in);
		//Welcome the user and get the input in feet
		System.out.println("Welcome to the feet to meters conversion application \n Please input the number of feet you want to convert into meters.");
		feet = input.nextDouble();
		
		//calculate feet to meters
		meters = feet * 0.305;
		
		//output the calculations
		System.out.println(feet + " feet is equal to " + meters + " meters. \n \n Thank you for using the feet to meters conversion application.");
		input.close();
	}

}

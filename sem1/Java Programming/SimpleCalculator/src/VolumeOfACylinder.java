import java.util.Scanner;

public class VolumeOfACylinder {

	public static void main(String[] args) {
		double radius;
		double length;
		double volume;
		double area;
		
		Scanner input = new Scanner(System.in);
		
		//Get input from the user
		System.out.println("Welcome to the Volume of a Cylinder application. \n Please input the radius of the cylinder.");
		radius = input.nextDouble();
		
		System.out.println("Please input the length of the cylinder.");
		length = input.nextDouble();
		
		//Calculate the volume and area of the cylinder
		area = radius * radius * 3.14;
		volume = area * length;
		
		//Output the calculations of area and volume
		System.out.println("The Area of the cylinder is \n" + area + "\n The volume of the cylinder is \n" + volume);
		
		input.close();

	}

}

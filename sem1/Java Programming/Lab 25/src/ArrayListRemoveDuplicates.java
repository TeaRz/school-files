import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListRemoveDuplicates {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		System.out.println("Please enter some numbers");
		removeDuplicate(list);

	}
	
	public static void removeDuplicate(ArrayList<Integer> list) {
		Scanner input = new Scanner(System.in);
		int value;
		
		for(int i = 0; i < 10; i++) {
			value = input.nextInt();
			
			if(!list.contains(value)) {
				list.add(value);
			}
		}
		System.out.println("The distinct integers are: " + list.toString());
		
		input.close();
	}

}

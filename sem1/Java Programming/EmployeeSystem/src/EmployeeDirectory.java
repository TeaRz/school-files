<<<<<<< Updated upstream
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

//have the program extend the application class
public class EmployeeDirectory extends Application {
	
	public static void main(String[] args) {
		
		Application.launch(args);
	}
	
	//create the start method
	@Override
	public void start(Stage primaryStage) throws Exception {
		/**
		 * Create a BorderPane
		 * We know that a BorderPane is divided up into different sections on the screen
		 * We will be using the top, center and bottom of the pane
		 */
		BorderPane pane = new BorderPane();
		
		/**
		 * When we want to display an image to the screen we do so in a
		 * ImageView. Image views store inside of them Image objects.
		 * In the following code we Create an ImageView and store within
		 * it an image names profilepic.jpg
		 * We then set the width and height of the image.
		 * The default measurement used in JavaFX is pixel
		 */
		Image pic = new Image("profilepic.jpg");
		ImageView profilePic = new ImageView(pic);
		profilePic.setFitHeight(350);
		profilePic.setFitWidth(400);
		
		Text name = new Text("Cai Filiault");
		name.setFont(Font.font("Times New Roman", FontWeight.BOLD, FontPosture.REGULAR, 40));
		name.setFill(Color.BLUE);
		
		Text description = new Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque a lacus ex. Praesent at elementum tellus."
				+ " Proin ut vestibulum lacus. Curabitur gravida sagittis blandit. Mauris tincidunt dapibus feugiat. Pellentesque pellentesque lorem arcu,"
				+ " nec commodo metus scelerisque eget. Maecenas enim qugue, finibus in malesuada eget, scelerisque eget nisl. Nunc rutrum magna augue, quis fringilla enim varius at. ");
		
		description.setFont(Font.font("Times New Roman", FontWeight.NORMAL, FontPosture.REGULAR, 16));
		description.setWrappingWidth(400);
		
		pane.setTop(profilePic);
		pane.setCenter(name);
		pane.setBottom(description);
		
		Scene scene = new Scene(pane, 400, 600);
		//add the scene to the stage
		primaryStage.setScene(scene);
		//set the stage title
		primaryStage.setTitle("Employee Directory");
		//show the stage
		primaryStage.show();
		
		
=======
//have the program extend the application class
public class EmployeeDirectory {
	
	//create the start method
	
	//create the appropriate panes to outline the look of the program
	
	//create a scene
	
	//add the panes to the scene
	
	//add the scene to the stage
	
	//set the stage title
	
	//show the stage
	
	public static void main(String[] args) {
		

>>>>>>> Stashed changes
	}

}

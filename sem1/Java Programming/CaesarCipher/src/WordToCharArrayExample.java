
public class WordToCharArrayExample {

	public static void main(String[] args) {
	
		String word = "encryption";
					// 0123456789
		//"encryption"
		//charAt();
		//to 'e' 'n' 'c' 'r' 'y' 'p' 't' 'i' 'o' 'n'
		char[] brokenDownWord = word.toCharArray();
		for(int i = 0; i < word.length(); i++) {
			
			System.out.print(brokenDownWord[i] + " ");
			System.out.println(word.charAt(i));
		}
	}

}


public class ConvertCharactertoNumber {

	public static void main(String[] args) {
		//created the letter a and stored in in letter
		char letter = 'j';
		//created an int and 
			//stored the int value of the letter 'a' in it
		int number = letter;
		//proved it by outputting 97 -> ascii dec value of 'a'
		System.out.println(number);
		int cipherNumber = number + 5;
		char cipherLetter = (char) cipherNumber;
		System.out.println(cipherLetter);
		//char -> int implicit casting
		//int -> char 
	}

}

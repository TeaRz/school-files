import java.util.StringTokenizer;

public class StringTokenizerExample {

	public static void main(String[] args) {
		String message = "Good better best never let it rest"
					   + " until your good is better than your "
					   + "betters best";
		StringTokenizer tokenizer = new StringTokenizer(message,
				" ");
		while(tokenizer.hasMoreTokens()) {
			System.out.println(tokenizer.nextToken());
		}

	}

}

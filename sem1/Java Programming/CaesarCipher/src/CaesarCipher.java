import java.util.Scanner;
import java.util.StringTokenizer;

public class CaesarCipher {

	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//We should welcome the user to the software
		//The software should ask if the user wants to encrypt(1) or decrypt(2)
		//the message
		System.out.println("Welcome to the secret message encoding software\n"
				+ "Would you like to encrypt (1) or decrypt (2) a message: \n"
				+ "Please enter 1 or 2:");
		int choice = input.nextInt();
		input.nextLine(); //Clear the enter key
		//The software should ask the user for the message
		System.out.println("Enter the message:");
		String message = input.nextLine();
		//The software should ask the user for the key
		System.out.println("Enter the key (1-25):");
		int key = input.nextInt();
		input.nextLine();
		//The software should then output the encrypted/decrypted message	
		System.out.println(cipher(message, choice, key));
		input.close();
	}
	
	/**
	 * This method encrypts and decrypts text
	 * @param text is the message you want to encrypt/decrypt
	 * @param mode is either (1) encrypt or (2) for decrypt
	 * @param key is the amount you want to shift the alphabet (1-25)
	 * @return the encrypted or decrypted text
	 * @author Cai Filiault
	 */
	public static String cipher(String text, int mode, int key) {
		text = text.toLowerCase();
		//String text = "hjjk brttew hllk"
		//This will be used to store the encrypted/decrypted result
		//store the result of encrypting or decrypting the parameter text
		String cipherText = ""; 
		// cipherText = "good better best"
		//Cipher word will store each word encrypted/decrypted one at a time
		String cipherWord = "";
		//cipherWord = "good", "better", "best"
		String token = "";
		StringTokenizer tokenizer = new StringTokenizer(text, " ");
		//While we still have more words, do the following:
		while(tokenizer.hasMoreTokens()) {
			//Reset the information inside of cipherWord
			cipherWord = "";
			//Grab the word to be encrypted/decrypted
			token = tokenizer.nextToken();
			//if mode is 1 (encrypt the word)
			if(mode == 1) {
				//Given the word "good", this loop will run 4 times 0 -> 3
				//good
				//0123
				for(int i = 0; i < token.length(); i++) {
					//Grab the current letter
					char letter = token.charAt(i);
					int numLetter = letter;
					//are we going past z
					if((numLetter + key) > 122) {
						int amountPastZ = (numLetter+key) - 122;
						cipherWord += (char) (96 + amountPastZ);
					}
					//if we are not going past z (then just shift the letter down the alphabet)
					else {
						cipherWord += (char)(letter + key);
					}
				}
			}
			//if we are decrypting the text
			else if (mode == 2) {
				for(int i = 0; i < token.length(); i++) {
					//Grab the current letter
					char letter = token.charAt(i);
					int numLetter = letter;
					//if the letter is before a
					if((numLetter-key) < 97) {
						int amountBeforeA = numLetter - key;
						cipherWord += (char) (122 - amountBeforeA);
					}
					else {
						cipherWord += (char) (letter - key);
					}
				}
			}
			cipherText += cipherWord + " ";
		}
		return cipherText;
	}
}

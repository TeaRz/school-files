
public class DisplayAverage {

	public static void main(String[] args) {
		double firstNum = 4;
		double secondNum = 5;
		double thirdNum = 10;
		
		double average = (firstNum + secondNum + thirdNum) / 3;
		
		System.out.printf("The average is %.2f", average);
	}

}

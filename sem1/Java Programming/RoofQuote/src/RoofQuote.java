import java.util.Scanner;

public class RoofQuote {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		//Inform the program of the relevant taxes
		final double HST = 1.13;
		
		//Allow the user to input the shingle cost
		System.out.println("Enter the cost of the shingles (per square foot)");
		double shingleCost = input.nextDouble();
		
		//Allow the user to input the square feet of the roof
		System.out.println("Enter the size of the roof (in square feet)");
		double customerRoof = input.nextDouble();
		
		//Allow the user to input the cost per square foot of installation
		System.out.println("Enter the cost of installation (per square foot)");
		double installationCost = input.nextDouble();
		
		//Calculate the subtotal and total after taxes
		//Calculate the cost of shingles
		//Calculate the labour for installation
		double subtotal = (shingleCost * customerRoof) + (customerRoof * installationCost);
		double total = (subtotal * HST);
		
		//We need to output the sub total and total after taxes 
		System.out.println("The subtotal is $" + subtotal);
		System.out.println("The total is $" + total);
		
		input.close();
	}

}

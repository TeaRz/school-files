import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) {
		/*
		 * We can create an ArrayList by using 
		 * ArrayList<datatype> name = new ArrayList<>()
		 * Please note the DataType should be of Object type
		 */
		ArrayList<String> cityList = new ArrayList<>();
		cityList.add("London");
		//["London"]
		System.out.println(cityList);
		cityList.add("Denver");
		cityList.add("Paris");
		cityList.add("Miami");
		//["london", "Denver", "Paris", "Miami"]
		cityList.add("Seoul");
		cityList.add("Tokyo");
		System.out.println(cityList);
		//Similar to the Array we can print and ArrayLists size
		System.out.println("The list has " + cityList.size() + " items");
		//We can search the ArrayList for a certain value
		System.out.println("Does the list contain Miami? " 
				+ cityList.contains("Miami"));
		System.out.println("Does the list contain Harrow? " 
				+ cityList.contains("Harrow"));
		System.out.println("What is at index location 0 ? " + cityList.get(0));
		System.out.println("What index location is London at? " 
				+ cityList.indexOf("London"));
		System.out.println(cityList);
		cityList.add(2, "Xian");
		System.out.println(cityList);
		cityList.remove("Miami");
		//cityList.remove(4);
		System.out.println(cityList);

		for(int i = 0; i < cityList.size(); i++) {
			System.out.println(cityList.get(i));
		}
	
	}

}

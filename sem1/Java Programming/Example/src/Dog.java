
public class Dog {
	//List the properties of a dog
	private int age; // real number for age
	private String eyeColor = "blue";
	private String coatType;
	private String breed;
	private String name;
	
	//Allow for the creation of a new dog
	//This is the constructor method
	//This will be called when new Dog() is called
	public Dog(String name, String breed) {
		this.name = name;
		this.breed = breed; 
	}
	public Dog(String name, String breed, String eyeColor, String coatType, int age) {
		this.name = name;
		this.breed = breed;
		this.eyeColor = eyeColor;
		this.coatType = coatType;
		this.age = age;
	}
	public void setAge(int age) {
		this.age = age;
	}



	public String getEyeColor() {
		return eyeColor;
	}



	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}



	public String getCoatType() {
		return coatType;
	}



	public void setCoatType(String coatType) {
		this.coatType = coatType;
	}



	public String getBreed() {
		return breed;
	}



	public void setBreed(String breed) {
		this.breed = breed;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

	public int getDogYears() {
		return age*7;
	}

	public void speak() {
		System.out.println("Bark bark! my name is " + name);
	}
	//actions the dog can perform

}

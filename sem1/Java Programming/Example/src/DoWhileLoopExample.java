import java.util.Random;
import java.util.Scanner;

public class DoWhileLoopExample {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Random r = new Random();
		int guess = 0;
		int randomNumber = 0;
		randomNumber = r.nextInt(10); //0-9 //1
		//do While loop works for an undetermined amount of time (at least once)
		//post check loop 
		do{
			System.out.println("Guess:");
			guess = input.nextInt();
			//the code in here will be run until the condition is false
		}while(guess != randomNumber);
		System.out.println("finished");
		//}
		input.close();

	}

}

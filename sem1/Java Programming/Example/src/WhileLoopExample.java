import java.util.Random;
import java.util.Scanner;

public class WhileLoopExample {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Random r = new Random();
		int guess = 0;
		int randomNumber = 0;
		randomNumber = r.nextInt(5); //0-9 //1
		guess = input.nextInt(); //ask the user to guess  //1
		//While loop works for an undetermined amount of time
		//While(boolean condition) {
		//pre check loop 
		while(guess != randomNumber) {
			System.out.println("Guess:");
			guess = input.nextInt();
			//the code in here will be run until the condition is false
		}
		System.out.println("finished");
		//}
		input.close();
	}

}


public class Example {

	public static void main(String[] args) {
		Dog dog1 = new Dog("Harry", "Boston Terrier");
		Dog dog2 = new Dog("Chuck", "Poodle", "Blue", "Long hair", 22);
		dog1.setName("Steve");
		System.out.println(dog1.getName());
		dog1.setAge(22);
		System.out.println("The dog is " + dog1.getDogYears() + " years old ");
		
	}

}


public class ForLoopExample {

	public static void main(String[] args) {
			
		//For loop runs for a set amount of time
		//for(create local variable; give boolean condition;
		//	a method to manipulate the local variable to meet the boolean condition)
		//{
			//as we enter the for loop for the first time
			//int i = 0; gets run
			// 10 < 10 - > 0, 1, 2, 3, 4, 5, 6, 7, 8 ,9 
			for(int i = 9; i >= 0; i--) {
				
				System.out.println(i);
			//every thing in here will be run while the boolean condition is true
			}
			//i++ (i = 9 + 1)
		//}
			//precheck
		
	}

}

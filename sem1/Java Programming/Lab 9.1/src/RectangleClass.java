import java.util.Scanner;

public class RectangleClass {

	public static void main(String[] args) {
		double width = 1;
		double height = 1;
		
		Scanner input = new Scanner(System.in);
		Area a = new Area();
		Perimeter p = new Perimeter();
		
		System.out.println("Please input width 4, height 40, for the first rectangle. \n Please input width 3.5, height 35.9, for the second rectangle");
		
		for(int i = 0; i < 2; i++) {
			
			if(i == 0) {
				width = input.nextDouble();
				a.setWidth(width);
				
				height = input.nextDouble();
				a.setHeight(height);
			} else if(i == 1) {
				width = input.nextDouble();
				p.setWidth(width);
				height = input.nextDouble();
				p.setHeight(height);
			}
			System.out.println("Rectange: " + (i+1) + ". width: " + width + ". height: " + height + ". area: " + a.getArea() + ". perimeter: " + p.getPerimeter());
		}

		input.close();
	}

}

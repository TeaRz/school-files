
public class Perimeter {
	private double width;
	private double height;
	
	public Perimeter() {
		width = 1;
		height = 1;
	}
	
	public Perimeter(double width, double height) {
		this.width = width;
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	public double getPerimeter() {
		return 2 * (width + height);
	}

	
}

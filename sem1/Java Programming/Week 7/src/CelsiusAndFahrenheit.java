public class CelsiusAndFahrenheit {

	public static void main(String[] args) {
		
		double fahrenheit;
		double celcius;
		double fahrenheitConversion;
		double celciusConversion;
		
		System.out.println("Celsius     Fahrenheit   |      Fahrenheit      Celsius \n"
						+ "__________________________________________________________");
		
		for(int i = 0; i < 10; i++) {
		    celcius = 40-i;
		    fahrenheit = 120-(i*10);
		    
		    celciusConversion = celsiusToFahrenheit(celcius);
		    fahrenheitConversion = fahrenheitToCelsius(fahrenheit);
		    
		    System.out.printf(" %.2f \t    %.2f  \t |   \t  %.2f      \t %.2f %n", celcius, celciusConversion, fahrenheit, fahrenheitConversion);
		}
		
	}
	
	public static double celsiusToFahrenheit(double celsius) {
			double fahrenheit = 0;
			
			fahrenheit = (9.0 / 5) * celsius + 32;
			
			return fahrenheit;
			
	}
	
	public static double fahrenheitToCelsius(double fahrenheit) {
		double celsius = 0;
		
		celsius = (5.0 / 9) * (fahrenheit - 32);
		
		return celsius;
	}

}

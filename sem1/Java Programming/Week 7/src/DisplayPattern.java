import java.util.Scanner;

public class DisplayPattern {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int n = input.nextInt();
		
		System.out.println(n);
		
		displayPattern(n);
		
		input.close();
	}
	
	public static void displayPattern(int n) {
		for(int i = 0; i < n; i++) {
			System.out.println();
		}
	}

}
 
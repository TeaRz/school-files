public class feetToMetersConversion {

	public static void main(String[] args) {
		
		double feet;
		double meters;
		double feetConversion;
		double metersConversion;
		
		System.out.println("Feet        Meters       |        Meters         Feet \n"
						+ "__________________________________________________________");
		
		for(int i = 0; i < 10; i++) {
		    meters = 20 + (i*5);
		    feet = 1+i;
		    
		    metersConversion = metersToFeet(meters);
		    feetConversion = feetToMeters(feet);
		    
		    System.out.printf(" %.1f \t    %.3f  \t |   \t  %.1f      \t %.3f %n", feet, feetConversion, meters, metersConversion);
		}
		
	}
	
	public static double metersToFeet(double meters) {
		double feet = 0;
			
		feet = 3.279 * meters;
			
		return feet;
			
	}
	
	public static double feetToMeters(double feet) {
		double meters = 0;
		
		meters = 0.305 * feet;
		
		return meters;
	}

}

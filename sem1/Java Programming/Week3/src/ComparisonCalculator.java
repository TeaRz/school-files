import java.util.Scanner;

public class ComparisonCalculator {

	public static void main(String[] args) {
		double firstNumber = 0;
		double secondNumber = 0;
		
		Scanner input = new Scanner(System.in);
		//Welcome the user to the software
		System.out.println("Welcome, this program makes the followin comparisons");
		//Tell the user what the software does
		System.out.println("1) if one number is larger than another \n"
				+ "2) if one number is smaller than another \n"
				+ "3) if two numbers are the same");
		//Get the user to input two numbers
		System.out.println("Please enter the first number for calculation");
		firstNumber = input.nextDouble();
		System.out.println("Please enter the second number for calculation");
		secondNumber = input.nextDouble();
		//use IF statements to determine the appropriate outputs
		if(firstNumber > secondNumber) {
			//everything in here will run if the statement evaluates to true
			System.out.println(firstNumber + " is larger than " + secondNumber);
		}
		else {
			//everything in here will run if the statement evaluates to false
			System.out.println(firstNumber + " is not larger than " + secondNumber);
		}
		if(firstNumber < secondNumber) {
			System.out.println(firstNumber + " is smaller than " + secondNumber);
		}
		else {
			System.out.println(firstNumber + " is not smaller than " + secondNumber);
		}
		if(firstNumber == secondNumber) {
			System.out.println(firstNumber + " is equal to " + secondNumber);
		}
		else {
			System.out.println(firstNumber + " is not equal to " + secondNumber);
		}
		//output the messages to the user
		input.close();
	}

}

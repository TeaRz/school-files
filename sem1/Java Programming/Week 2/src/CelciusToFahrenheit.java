import java.util.Scanner;

public class CelciusToFahrenheit {

	public static void main(String[] args) {
		double celsius;
		double fahrenheit;
		
		//Welcome the user
		System.out.println("Welcome to the Celsius to Fahrenheit conversion application \n Please enter a number to be converted into fahrenheit: ");
		
		//input from the user
		Scanner input = new Scanner(System.in);
		celsius = input.nextDouble();
		
		//calculate celsius to fahrenheit
		fahrenheit = (9.0 / 5) * celsius + 32;
		
		//output the calculations
		System.out.println("You input " + celsius + " celsius to be converted into fahrenheit.");
		System.out.println(fahrenheit + " is the conversion from what you inputed.");
		
		input.close();
	}

}

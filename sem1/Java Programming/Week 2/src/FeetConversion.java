import java.util.Scanner;

public class FeetConversion {

	public static void main(String[] args) {
		
		double feet;
		double meters = 0.305;
		double result;
		
		//Welcome the user
		System.out.println("Welcome to the feet to meters calculator");
		
		//input from the user
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter the amount of feet you want to convert into meters");
		feet = input.nextDouble();
		
		//calculate the results
		result = feet * meters;
		
		//output the calculations
		System.out.println("You input " + feet + " to be converted into meters.");
		System.out.println(feet + " = " + result);
		
	}

}

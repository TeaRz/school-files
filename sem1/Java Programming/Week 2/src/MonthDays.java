import java.util.Scanner;

public class MonthDays {
	
	//Name: Cody Pedro
	//Student Number:
	//Program Name: MonthDays
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int choice = 0;
		
		//Welcome the user
		System.out.println("Welcome to the days of the month software, enter a number and the program will tell you the number of days in that month \n 1) Janurary: \n 2) February \n 3) March \n 4) April \n 5) May \n 6) June \n 7) July \n 8) August \n 9) September \n 10) October \n 11) November \n 12) December");
		//input from the user
		choice = input.nextInt();
		switch (choice){
		case 1:
			System.out.println("January has 31 Days");
			break;
		case 2:
			System.out.println("February has 28 Days");
			break;
		case 3:
			System.out.println("March has 31 Days");
			break;
		case 4:
			System.out.println("April has 30 Days");
			break;
		case 5:
			System.out.println("May has 31 Days");
			break;
		case 6:
			System.out.println("June has 30 Days");
			break;
		case 7:
			System.out.println("July has 31 Days");
			break;
		case 8:
			System.out.println("August has 31 Days");
			break;
		case 9:
			System.out.println("September has 30 Days");
			break;
		case 10:
			System.out.println("October has 31 Days");
			break;
		case 11:
			System.out.println("November has 30 Days");
			break;
		case 12:
			System.out.println("December has 31 Days");
			break;
		}
	}

}

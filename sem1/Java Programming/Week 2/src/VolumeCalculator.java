import java.util.Scanner;

public class VolumeCalculator {
	
	public static void main(String[] args) {
		double area;
		double radius;
		double length;
		double volume;
		
		double pi = 3.14;
		
		//Welcome the user
		System.out.println("Welcome to the  Volume Calculator");
		
		//input from the user
		System.out.println("Please enter the radius of the cylinder");
		Scanner input = new Scanner(System.in);
		radius = input.nextDouble();
		
		System.out.println("Please enter the length of the cylinder");
		length = input.nextDouble();
		
		//calculations
		area = radius * radius * pi;
		
		volume = area * length;
		
		//output the calculations
		System.out.println("The area of the cyclinder is " + area);
		System.out.println("The volume of the cylinder is " + volume);
		
	}

}

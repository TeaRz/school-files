import java.util.Random;

public class RandomExample {

	public static void main(String[] args) {
		Random r = new Random(100); //100 is the seed if there is no seed it takes the time
		System.out.println(r.nextInt(10) + 1); //0 - 9 +1 (1 - 10)
		System.out.println(r.nextInt(10) + 1); //0 - 9 +1 (1 - 10)
		System.out.println(r.nextInt(10) + 1); //0 - 9 +1 (1 - 10)
		System.out.println(r.nextInt(10) + 1); //0 - 9 +1 (1 - 10)
		System.out.println(r.nextInt(10) + 1); //0 - 9 +1 (1 - 10)

	}

}

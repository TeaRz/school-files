import java.util.Random;
import java.util.Scanner;

public class RandomNumberGame {

	public static void main(String[] args) {
		
		Random r = new Random();
		
		int choice;
		int rounds;
		int randomNumber = 0;
		int guess;
		int score = 0;
		int numGuess = 0;
		
		Scanner input = new Scanner(System.in);
		
		//Welcome the user to the software
		System.out.println("Welcome to the random number game");
		
		//Allow the user to select the game difficulty
		System.out.println("Select a game difficulty: \n 1) Easy \n 2) Medium \n 3) Hard \n Please enter a number (1 - 3): \n");
		choice = input.nextInt();
		
		//Allow the user to enter the number of rounds
		System.out.println("Please enter the number of rounds you would like to play");
		rounds = input.nextInt();
		
		//The game will then generate a random number
		for(int i = 0; i < rounds; i++) {
			
			score += numGuess; //Add the num guess to the score
			numGuess = 0; //Reset the num guess for the round
			System.out.println("Round Number: " + (i+1));
			
			switch(choice) {
			case 1:
					randomNumber = r.nextInt(10)+1; // 1 - 10
				break;
			case 2:
					randomNumber = r.nextInt(25)+1; // 1 - 25
				break;
			case 3:
					randomNumber = r.nextInt(50)+1; // 1 - 50
				break;
			}
			
			//Continually ask the user to enter a number until the correct one is guessed
			do {
				switch(choice) {
				case 1:
					System.out.println("Please enter a number between 1 - 10:");
					break;
				case 2:
					System.out.println("Please enter a number between 1 - 23:");
					break;
				case 3:
					System.out.println("Please enter a number between 1 - 50:");
				}
				
				guess = input.nextInt();
				
				if(guess == randomNumber) {
					System.out.println("Correct!");
				}
				else {
					System.out.println("Incorrect! Try again");
				}
				numGuess++;
			} while(guess != randomNumber);
		}
		score+= numGuess;
		System.out.println("Your score is: " + score);
		//run the game again the desired amount of rounds
		//Keep track of the score
		//Output the score to the screen
		input.close();

	}

}

CSS Colors

color: black
code:
    hex: #000
    rgb: 0,0,0
    hsl: 0, 0%, 0%
    
color: blue
code:
    hex: #0000ff
    rgb: 0,0,255
    hsl: 240, 100%, 50%

colour: yellow
code:
    hex: #ffff00
    rgb: 255, 255, 0
    hsl: 60, 100%, 50%

color: orange
code:
    hex: #ffa500
    rgb: 255, 165, 0
    hsl: 39, 100%, 50%
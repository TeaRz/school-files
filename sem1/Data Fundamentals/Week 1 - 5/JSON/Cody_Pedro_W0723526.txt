{
	"rareItems": [{
		
		"title": "Rare Jazz Collectibles",
		"subtitle": "New Offerings",
		
		"item": {
			"artist": "Louis Armstrong",
			"record": "Satchmo Serenades",
			"year": "1952",
			"label": "Decca",
			"condition": "VG+",
			"priceUS": $50,
		}
	}]
}
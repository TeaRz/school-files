/*Task 1 - Create a top grossing movie table*/
CREATE TABLE top_movies (
	rank INTEGER PRIMARY KEY NOT NULL,
	title VARCHAR(35) NOT NULL,
	gross VARCHAR(20) NOT NULL,
	year INTEGER NOT NULL
)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(1, "Gone with the Wind", 3440000000, 1939)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(2, "Avatar", 3020000000, 2009)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(3, "Star Wars", 2825000000, 1977)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(4, "Titanic", 2516000000, 1997)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(5, "The Sound of Music", 2366000000, 1965)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(6, "E.T. the Extra-Terrestrial", 2310000000, 1982)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(7, "The Ten Commandments", 2187000000, 1956)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(8, "Doctor Zhivago", 2073000000, 1965)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(9, "Jaws", 2027000000, 1975)

INSERT INTO top_movies
(rank, title, gross, year)
VALUES
(10, "Snow White and the Seven Dwarfs", 1819000000, 1937)


/*Task #2 - Create, Insert*/
CREATE TABLE contact_information (
	id INTEGER PRIMARY KEY NOT NULL,
	companyName VARCHAR(35) NOT NULL,
	contact VARCHAR(20) NOT NULL,
	position VARCHAR(20) NOT NULL,
	address VARCHAR(35) NOT NULL,
	city VARCHAR(15) NOT NULL,
	state VARCHAR(6),
	postal VARCHAR(10) NOT NULL,
	country VARCHAR(25) NOT NULL,
	phone VARCHAR(10) NOT NULL,
	fax VARCHAR(10),
	email VARCHAR(30)
)

INSERT INTO contact_information
(id, companyName, contact, position, address, city, postal, country, phone)
VALUES
(1, "exotic Liquids", "Charlotte Cooper", "Purchasing Manager", "49 Gilbert St.", "London", "EC1 4SD", "United Kingdom", "1715552222");

INSERT INTO contact_information
(id, companyName, contact, position, address, city, state, postal, country, phone, email)
VALUES
(2, "New Orleans Cajun Delights", "Shelley Burke", "Order Administrator", "P.O. Box 78934", "New Orleans", "LA", "70117", "United States", "1005554822", "www.cajundelights.co");

INSERT INTO contact_information
(id, companyName, contact, position, address, city, state, postal, country, phone, fax)
VALUES
(3, "Grandma Kelly's Homestead", "Regina Murphy", "Sales Representative", "707 Oxford Rd", "Ann Arbor", "MI", "48104", "United States", "3135555735", "3135553349");

INSERT INTO contact_information
(id, companyName, contact, position, address, city, postal, country, phone)
VALUES
(4, "Tokyo Traders", "Yoshi Nagase", "Marketing Manager", "9-8 Sekimai MUnited Statesshino-shi", "Tokyo", "100", "Japan", "0335555011")


/* Updating */
UPDATE contact_information
SET contact = "Mario Luigi"
WHERE id = 4

UPDATE contact_information
SET position = "Plumber"
WHERE id = 4

UPDATE contact_information
SET email = "www.grankelly.org"
WHERE id = 3

DELETE FROM contact_information
WHERE id = 1

UPDATE contact_information
SET address = "1234 Main St"
WHERE id = 2;

UPDATE contact_information
SET city = "Springfield"
WHERE id = 2;

UPDATE contact_information
SET state = "IL"
WHERE id = 2;

UPDATE contact_information
SET postal = 19382
WHERE id = 2;

UPDATE contact_information
SET contact = "Homer Simpson"
WHERE id = 2;

UPDATE contact_information
SET position = "Purchasing Engineer"
WHERE id = 2;

UPDATE contact_information
SET phone = "5555555555"
WHERE id = 2;

UPDATE contact_information
SET email = "www.cajundelights-midwest.com"
WHERE id = 2
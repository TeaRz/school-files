/* Question 1 */
SELECT au_fname, au_lname
FROM authors
WHERE phone = 212;

/* Question 2 */
SELECT *
FROM titles
WHERE pubdate LIKE '%05%';

/* Question 3 */
SELECT DISTINCT pub_id
FROM titles

/* Question 4 */
SELECT title_name, CHARACTER_LENGTH(title_name)
FROM titles
ORDER BY CHARACTER_LENGTH(title_name) DESC;

/* Question 5 */
SELECT au_fname, au_lname
FROM authors
ORDER BY au_fname DESC;

/* Question 6 */
SELECT title_name, MAX(price) AS Most_Expensive_Book
FROM titles;

/* Question 7 NEEDS TO BE WORKED ON*/
SELECT MIN(price) AS Least_Expensive_Book , title_name
FROM titles
WHERE type = 'biography';

/* Question 8 */
SELECT AVG(price) AS 'Average_Selling_Price)'
FROM titles;

/* Question 9 */
SELECT AVG(price) AS 'Average_Selling_Price'
FROM titles
WHERE type = 'history';

/* Question 10 */
SELECT AVG(pages) AS 'Average_pages'
FROM titles
WHERE type = 'biography' AND price > '15';

/* Question 11 */
SELECT SUM(pages)
FROM titles
WHERE type = 'children' AND price > '10';

/* Question 12 */
SELECT SUM(pages)
FROM titles;

/* Question 13 */
SELECT COUNT(*)
FROM titles
WHERE sales > 500 AND type = 'biography';

/* Question 14 */
SELECT COUNT(*)
FROM titles
WHERE type = 'psychology' AND pages > '1000' OR type = 'psychology' AND price > '7';

/* Question 15 */
SELECT type, COUNT(type)
FROM titles
GROUP BY type;

/* Question 16 */
SELECT pub_id, type, COUNT(type)
FROM titles
GROUP BY pub_id, type;

/* Question 17  NEED HELP */
SELECT DISTINCT emp_name, sales
FROM empsales, employees
WHERE sales >= 500
ORDER BY emp_name DESC;

/* Question 18 */
/* Question 19 */
/* Question 20 */
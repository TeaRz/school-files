
/*Task 1 - Create a table*/
CREATE TABLE my_employee (
	employeeId INTEGER PRIMARY KEY NOT NULL,
	firstName VARCHAR(25) NOT NULL,
	lastName VARCHAR(30) NOT NULL,
	address VARCHAR(10) NOT NULL,
	birthday VARCHAR,
	salary DECIMAL DEFAULT 45000,
	maritalStatus VARCHAR(1),
	yearsOfService INTEGER NOT NULL,
	department VARCHAR(20)
);

/*Task 2 - Create a Second table*/
CREATE TABLE computers (
	computerId INTEGER PRIMARY KEY NOT NULL,
	modelNumber VARCHAR(6) NOT NULL,
	computerType VARCHAR(10) NOT NULL,
	serviceTag VARCHAR(15) NOT NULL,
	assignedTo VARCHAR(25) NOT NULL
);

/*Task 3 - Create a Final table*/
CREATE TABLE family_contact (
	memberId INTEGER PRIMARY KEY NOT NULL,
	firstName VARCHAR(20) NOT NULL,
	lastName VARCHAR(20) NOT NULL,
	email varchar(35),
	mailAddress VARCHAR(30) NOT NULL,
	city VARCHAR(15) NOT NULL,
	homePhone VARCHAR(10) NOT NULL,
	workPhone VARCHAR(10),
	cellPhone VARCHAR(10)
);

/*Task 4 - ALTER THE TABLE*/
ALTER TABLE my_employee
ADD COLUMN bonus DECIMAL(5,2) NOT NULL,
MODIFY COLUMN address VARCHAR(50) NOT NULL,
MODIFY COLUMN maritalStatus VARCHAR(1) NOT NULL

/*Task 5 - Copy and Drop*/
CREATE TABLE my_employeeBackup AS
SELECT * FROM my_employee

ALTER TABLE my_employeeBackup
DROP COLUMN maritalStatus;

CREATE TABLE my_employeeCorrupted AS
SELECT * FROM my_employeeBackup

DROP TABLE my_employeeCorrupted

ALTER TABLE my_employeeBackup
ADD COLUMN manager VARCHAR(20) NOT NULL
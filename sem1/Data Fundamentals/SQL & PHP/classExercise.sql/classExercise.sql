CREATE TABLE publishers(
    pubId CHAR(3) PRIMARY KEY,
    pubName VARCHAR(20) NOT NULL,
    city VARCHAR(20),
    province CHAR(2),
    country VARCHAR(100)
);


CREATE TABLE titles(
    title_id CHAR(3) PRIMARY KEY,
    title_name VARCHAR(40) NOT NULL,
    pages INTEGER,
    price DECIMAL(5,2),
    pubdate DATE
);

CREATE TABLE royalties(
    royalty_id CHAR(3) PRIMARY KEY, 
    advance DECIMAL(9,2),
    titles_id CHAR(3) REFERENCES titles(title_id)
);

CREATE TABLE publishers_copy AS
SELECT * FROM publishers;


ALTER TABLE publishers
ADD COLUMN email_address VARCHAR(50);

ALTER TABLE publishers
MODIFY COLUMN province CHAR(2) DEFAULT 'ON';

ALTER TABLE publishers
DROP COLUMN province;

ALTER TABLE publishers
DROP COLUMN city, country;

DROP TABLE publishers_copy;

/*Question 1*/ 
CREATE TABLE books(
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(100) NOT NULL,
    genre VARCHAR(30) NOT NULL,
    pages INTEGER NOT NULL,
    price DECIMAL(5,2) NOT NULL,
    pub_date DATE NOT NULL,
    ebook CHAR(1) NOT NULL
);

DROP TABLE books;

ALTER TABLE books
ADD COLUMN author VARCHAR(100) NOT NULL;

ALTER TABLE books
DROP COLUMN ebook;









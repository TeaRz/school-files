-- Task 1 --
SELECT titles.type, publishers.pub_name
FROM titles INNER JOIN publishers
ON titles.pub_id = publishers.pub_id;

-- Task 2--
SELECT t.type, t.title_name, p.pub_name
FROM titles AS t INNER JOIN publishers AS p
ON t.pub_id = p.pub_id
WHERE t.type = 'history';

-- Task 3 --
SELECT t.price, t.title_name, p.pub_name
FROM titles AS t INNER JOIN publishers AS p
ON t.pub_id = p.pub_id
WHERE t.price < '12';

-- Task 4 --
SELECT t.title_name, p.pub_name, p.city, p.state, p.country
FROM titles AS t INNER JOIN publishers AS p
ON t.pub_id = p.pub_id
WHERE p.country != 'USA' OR p.state = 'CA';

-- Task 5 --
SELECT t.title_name, a.advance
FROM titles AS t INNER JOIN royalties AS a
ON t.title_id = a.title_id;

-- Task 6 --
SELECT emp.sales, employees.emp_name
FROM employees INNER JOIN empsales AS emp
ON employees.emp_id = emp.emp_id
WHERE employees.emp_name = 'Lord Copper';

-- Task 7 --
SELECT emp.sales, employees.emp_name
FROM employees INNER JOIN empsales AS emp
ON employees.emp_id = emp.emp_id
WHERE emp.sales >= '500';

-- Task 8 --
SELECT t.title_name, r.royalty_rate
FROM titles AS t INNER JOIN royalties AS r
ON t.title_id = r.title_id
WHERE r.royalty_rate > '0.05'
ORDER BY r.royalty_rate DESC;

-- Task 9 --
SELECT t.title_name, r.advance, p.pub_name
FROM titles AS t INNER JOIN publishers AS p
ON t.pub_id = p.pub_id
INNER JOIN royalties AS r
ON t.title_id = r.title_id
WHERE r.advance >= '1000000.00';

-- Task 10 --
SELECT p.pub_id, p.pub_name, COUNT(t.type)
FROM titles AS t INNER JOIN publishers AS p
ON t.pub_id = p.pub_id
GROUP BY p.pub_name;

-- Task 11 --
SELECT CONCAT(a.au_fname, ' ', a.au_lname) AS 'Author Name', t.title_name
FROM authors AS a INNER JOIN title_authors AS ta
ON a.au_id = ta.au_id
INNER JOIN titles AS t
ON ta.title_id = t.title_id
ORDER BY a.au_fname;

-- Task 12 --
SELECT CONCAT(a.au_fname, ' ', a.au_lname) AS 'Author Name'
FROM authors AS a INNER JOIN title_authors AS ta
ON a.au_id = ta.au_id
INNER JOIN titles AS t
ON ta.title_id = t.title_id
WHERE t.type = 'children';

-- Task 13 -- 
SELECT CONCAT(a.au_fname, ' ', a.au_lname) AS 'Author Name', t.title_name, r.advance
FROM authors AS a INNER JOIN title_authors AS ta
ON a.au_id = ta.au_id
INNER JOIN titles AS t
ON ta.title_id = t.title_id
INNER JOIN royalties AS r
ON t.title_id = r.title_id
WHERE r.advance > '1000.00'
ORDER BY r.advance DESC;

-- Task 14 --
SELECT CONCAT(a.au_fname, ' ', a.au_lname) AS 'Author Name', p.pub_name
FROM authors AS a INNER JOIN title_authors AS ta
ON a.au_id = ta.au_id
INNER JOIN titles AS t
ON ta.title_id = t.title_id
INNER JOIN publishers AS p
ON t.pub_id = p.pub_id
WHERE pub_name LIKE '%Press';

-- Task 15 --
SELECT CONCAT(a.au_fname, ' ', a.au_lname) AS 'Author Name', t.title_name, p.pub_name
FROM authors AS a INNER JOIN title_authors AS ta
ON a.au_id = ta.au_id
INNER JOIN titles AS t
ON ta.title_id = t.title_id
INNER JOIN publishers AS p
ON p.pub_id = t.pub_id;

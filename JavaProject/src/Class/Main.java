package Class;

import Scenes.IntroScene;
import Scenes.MainMenuScene;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
public static Stage mainStage;
	
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Make the primaryStage global so any class can change scenes
		
		//String musicFile = "bin/Music/farmMusic.mp3";

		//Media sound = new Media(new File(musicFile).toURI().toString());
		//MediaPlayer mediaPlayer = new MediaPlayer(sound);
		//mediaPlayer.play();
		
		mainStage = primaryStage;
		mainStage.setScene(new IntroScene());
		mainStage.setTitle("Simple Game Example");
		mainStage.show();
		
	}
}

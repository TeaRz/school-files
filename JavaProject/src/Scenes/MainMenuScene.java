package Scenes;

import Panes.MainMenuPane;
import javafx.scene.Scene;

public class MainMenuScene extends Scene {

	public MainMenuScene() {
		super(new MainMenuPane(), 1000, 750);
	}
	
}

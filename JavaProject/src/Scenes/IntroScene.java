package Scenes;

import Panes.IntroPane;
import javafx.scene.Scene;

public class IntroScene extends Scene{
	
	public IntroScene() {
		super(new IntroPane(), 600, 500);
	}
}

package Panes;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public final class CardTile extends  ImageView {
    private boolean cardRevealed;
    private final Image cardBack;
    private final Image cardFront;
    
    public CardTile(Image cardBack, Image cardFront) {
        this.cardBack = cardBack;
        this.cardFront = cardFront;
        
        
        this.setImage(cardBack);
        cardRevealed = false;
    }

    public void showCardBack() {
        this.setImage(cardBack);
        cardRevealed = false;
    }

    public void showCardFront() {
        this.setImage(cardFront);
        
        cardRevealed = true;
    }    

    public boolean cardUp() {
        return cardRevealed;
        
    }
}


package Panes;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Class.Main;
import Scenes.GamesScene;
import Scenes.MainMenuScene;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class SpellingGame extends BorderPane {
	
	Random rand = new Random();
	public int r = rand.nextInt(100);
	
	public String enteredWord ="";
	public Text temp = new Text();
	public String temp2 = "";
	
	static int interval;
	static Timer timer;
	
	public int secs = 2;
	public int right = 0;
	public int wrong = 0;

	public String wordArray[] = {"View", "Coin", "Pony", "Mist", "Die", "Tone", "Tile", "Beer", "Head"
			, "Lock", "Leg", "Grip", "Pair", "Shy", "Skin", "Swim", "Poll", "End", "Lead", "Make"
			, "Try", "Sink", "Case", "Just", "Knot", "Mold", "Calf", "Slow", "Oven", "Soil", "Leak"
			, "Mud", "Fat", "Bar", "Pig", "Zone", "Seed", "Army", "Kid", "Bet", "Rent", "Push"
			, "Coat", "Bird", "Get", "Rule", "Ward", "Bee", "Wild", "Toss", "Hero", "Land"
			, "Tree", "West", "Bat", "Acid", "Vein", "Live", "Thaw", "Egg", "Dive", "Ball", "Sell"
			, "Huge", "Cry", "Cash", "Spy", "Cafe", "Warm", "Clue", "Note", "Lake", "Harm", "Duty"
			, "Wrap", "Ash", "Log", "Pen", "Knee", "Hike", "Cut", "Sand", "Pest", "Node", "Know"
			, "Lose", "Suit", "Law", "Corn", "Feed", "Quit", "Lean", "Gem", "Crew", "Flat", "Date", "Gate"
			, "Fuel", "Dump", "Snub"};
	
	public String word = wordArray[r];
	public TextField text = new TextField();
	public Text lb1 = new Text("");
	public Label rightLabel = new Label("Right");
	public Label wrongLabel = new Label("Wrong");
	public Label rightScore = new Label("0");
	public Label wrongScore = new Label("0");
	public Label quitLabel = new Label("Quit");
	public Label resumeLabel = new Label("Resume");
	public Label timerLabel = new Label("10");
	
	public Image buttonHover = new Image("Images/ButtonHover.png");
	public Image buttonClicked = new Image("Images/ButtonClicked.png");
	public Image buttonNormal = new Image("Images/ButtonNormal.png");
	
	public ImageView button = new ImageView(buttonNormal);

	
	public TranslateTransition tt = new TranslateTransition();
	
	public Pane quitPane = new Pane();
	
	public boolean paused = false;
	public static boolean gameFinished = false;
	
	public SpellingGame() {
		
		Image buttonPauseNormal = new Image("Images/ButtonNormal.png");
		Image buttonPauseHover = new Image("Images/ButtonHover.png");
		Image buttonPauseClicked = new Image("Images/ButtonClicked.png");
		
		ImageView buttonPause = new ImageView(buttonPauseNormal);
		buttonPause.setFitHeight(45);
		buttonPause.setFitWidth(45);
		
		Pane pane = new Pane();
		pane.getChildren().addAll(lb1, text, rightLabel, wrongLabel, rightScore, wrongScore, buttonPause);
		
		//Pause Menu Scene
				StackPane quitButton = new StackPane();
				StackPane resumeButton = new StackPane();
				
				button.setFitWidth(20);
				button.setFitHeight(10);

				ImageView buttonQuit = new ImageView(buttonNormal);
				buttonQuit.setFitHeight(35);
				buttonQuit.setFitWidth(105);
				
				quitButton.getChildren().addAll(buttonQuit, quitLabel);
				
				ImageView buttonResume = new ImageView(buttonNormal);
				buttonResume.setFitHeight(35);
				buttonResume.setFitWidth(105);
				
				resumeButton.getChildren().addAll(buttonResume, resumeLabel);
				
				Pane pauseMenu = new Pane();
				pauseMenu.getChildren().addAll(quitButton, resumeButton);
				
				quitButton.setLayoutX(450);
				quitButton.setLayoutY(350);
				
				resumeButton.setLayoutX(450);
				resumeButton.setLayoutY(300);
		
		timer = new Timer();
		interval = secs;
		
		timer.scheduleAtFixedRate(new TimerTask(){
			public void run() {
				System.out.println(setInterval());
				if(interval == 0) {
					gameFinished();
				}
			}
		}, 1000, 1000);
		
		lb1.setText(word);
		lb1.setFont(Font.font(null, FontWeight.BOLD, 32));
		lb1.setFill(Color.BLACK);

		tt.setNode(lb1);
		tt.setFromX(1000);
		tt.setToX(400);
		tt.setDuration(Duration.millis(3000));
		
		tt.play();

		
		
		lb1.setLayoutX(100);
		lb1.setLayoutY(375);
		
		text.setLayoutX(450);
		text.setLayoutY(700);
		
		rightLabel.setLayoutX(10);
		rightLabel.setLayoutY(675);
		rightScore.setLayoutX(50);
		rightScore.setLayoutY(675);
		
		wrongLabel.setLayoutX(10);
		wrongLabel.setLayoutY(700);
		wrongScore.setLayoutX(50);
		wrongScore.setLayoutY(700);
		
		buttonPause.setLayoutX(10);
		buttonPause.setLayoutY(10);
		
		this.setCenter(pane);
		
		
		
		text.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				
				enteredWord = newValue;
				
				temp.setText(word.substring(0, enteredWord.length()));
				
				if(enteredWord.length() <= word.length() && enteredWord.length() > 0) {
					temp = word.substring(0, enteredWord.length());
					
					if(enteredWord.equals(temp)) {
						lb1.setFill(Color.GREEN);
					} else {
						lb1.setFill(Color.RED);
					}
				} else if(enteredWord.equals("")) {
					lb1.setFill(Color.BLACK);
				}
			}
		});
		
		text.setOnKeyPressed(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				if(event.getCode().equals(KeyCode.ENTER) && enteredWord != "" || event.getCode().equals(KeyCode.ENTER) && enteredWord != null ) {
					if(enteredWord.equals(temp)) {
						System.out.println("The word that was entered is Correct");
						right++;
						rightScore.setText(right +"");
						enteredWord="";
					} else {
						System.out.println("The word that was entered is Wrong");
						wrong++;
						wrongScore.setText(wrong + "");
						enteredWord="";
					}
					
					text.setText("");
					r = rand.nextInt(100);
					lb1.setText(word = wordArray[r]);
					lb1.setFill(Color.BLACK);
					tt.stop();
					tt.play();
					
				}
				
			}
		});
		
		
		//Pause Button Events
		buttonPause.addEventHandler(MouseEvent.MOUSE_ENTERED,
				        new EventHandler<MouseEvent>() {
				        	@Override
				        	public void handle(MouseEvent e) {
				        		buttonPause.setImage(buttonPauseHover);
				        	}
				        });
			
		buttonPause.addEventHandler(MouseEvent.MOUSE_EXITED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonPause.setImage(buttonPauseNormal);
							}
						});
		buttonPause.addEventHandler(MouseEvent.MOUSE_CLICKED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonPause.setImage(buttonPauseClicked);
								timer.purge();
								int currentTime = interval;
								System.out.println("The game paused on " + currentTime + " seconds");
								pane.getChildren().add(pauseMenu);
								tt.pause();
								text.setEditable(false);
							}
						});
		
		//Pause Button Events
				resumeButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
						        new EventHandler<MouseEvent>() {
						        	@Override
						        	public void handle(MouseEvent e) {
						        		buttonResume.setImage(buttonPauseHover);
						        	}
						        });
					
				resumeButton.addEventHandler(MouseEvent.MOUSE_EXITED,
								new EventHandler<MouseEvent>() {
									@Override
									public void handle(MouseEvent e) {
										buttonResume.setImage(buttonPauseNormal);
									}
								});
				resumeButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
								new EventHandler<MouseEvent>() {
									@Override
									public void handle(MouseEvent e) {
										buttonResume.setImage(buttonPauseClicked);
										timer.scheduleAtFixedRate(new TimerTask(){
										public void run() {
												System.out.println(setInterval());
											}
										}, 1000, 1000);
										System.out.println("The game is unpaused");
										pane.getChildren().remove(pauseMenu);
										tt.play();
										text.setEditable(true);
									}
								});
				//Pause Button Events
				quitButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
						        new EventHandler<MouseEvent>() {
						        	@Override
						        	public void handle(MouseEvent e) {
						        		buttonQuit.setImage(buttonPauseHover);
						        	}
						        });
					
				quitButton.addEventHandler(MouseEvent.MOUSE_EXITED,
								new EventHandler<MouseEvent>() {
									@Override
									public void handle(MouseEvent e) {
										buttonQuit.setImage(buttonPauseNormal);
									}
								});
				quitButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
								new EventHandler<MouseEvent>() {
									@Override
									public void handle(MouseEvent e) {
										Main.mainStage.setScene(new MainMenuScene());
										timer.cancel();
									}
								});
				
				
	}
	
	private void gameFinished() {
		timer.purge();
		int currentTime = interval;
		System.out.println("The game paused on " + currentTime + " seconds");
		Main.mainStage.setScene(new MainMenuScene());
		tt.pause();
		text.setEditable(false);
		
	}

	private static final int setInterval() {
	if (interval == 1) 
		timer.cancel();
	return --interval;
	}
	
	
}

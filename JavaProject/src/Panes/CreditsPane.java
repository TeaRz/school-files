package Panes;

import java.io.File;

import Class.Main;
import Scenes.MainMenuScene;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class CreditsPane extends BorderPane {

	public CreditsPane() {
		
		String musicFile = "src/Music/piano.mp3";
		Image facebook = new Image("Images/facebook.png", true);
		Image twitter = new Image("Images/twitter.png", true);
		Image google = new Image("Images/google.png", true);
		
		Image buttonBackNormal = new Image("Images/ButtonBackNormal.png");
		Image buttonBackClicked = new Image("Images/ButtonBackClicked.png");
		Image buttonBackHover = new Image("Images/ButtonBackHover.png");
		
		Media sound = new Media(new File(musicFile).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(sound);
		mediaPlayer.play();
		Image title = new Image("Images/Credits.png", true);
		ImageView titleT = new ImageView(title);
		titleT.setFitHeight(80);
		titleT.setFitWidth(180);
		titleT.setLayoutX(200);
		titleT.setLayoutY(10);
		
		
		Image names = new Image("Images/Names.png", true);
		ImageView oNames = new ImageView(names);
		oNames.setFitHeight(120);
		oNames.setFitWidth(250);
		oNames.setLayoutX(180);
		oNames.setLayoutY(90);

		
		Image para = new Image("Images/Credits2.png", true);
		ImageView cPara = new ImageView(para);
		cPara.setFitHeight(80);
		cPara.setFitWidth(350);
		cPara.setLayoutX(125);
		cPara.setLayoutY(400);
		//Facebook
		Button face = new Button();
		BackgroundImage biFace = new BackgroundImage(facebook, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(facebook.getWidth(), facebook.getHeight(), true, true, true, false));
	    Background backFace = new Background(biFace);
	    face.setMinSize(80, 37);
	    face.setMaxSize(80, 37);
		face.setLayoutX(500);
		face.setLayoutY(20);
		face.setBackground(backFace);
		//Twitter
		Button twit = new Button();
		BackgroundImage biTwit = new BackgroundImage(twitter, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(twitter.getWidth(), twitter.getHeight(), true, true, true, false));
	    Background backTwit = new Background(biTwit);
	    twit.setMinSize(80, 37);
	    twit.setMaxSize(80, 37);
		twit.setLayoutX(460);
		twit.setLayoutY(20);
		twit.setBackground(backTwit);
		//Google
		Button goog = new Button();
		BackgroundImage biGoog = new BackgroundImage(google, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(google.getWidth(), google.getHeight(), true, true, true, false));
	    Background backGoog = new Background(biGoog);
	    goog.setMinSize(80, 37);
	    goog.setMaxSize(80, 37);
		goog.setLayoutX(540);
		goog.setLayoutY(20);
		goog.setBackground(backGoog);
		
		
		//Back button
		ImageView buttonBack = new ImageView(buttonBackNormal);
		buttonBack.setFitHeight(45);
		buttonBack.setFitWidth(45);
		
		Pane root = new Pane();
		Scene scene = new Scene(root, 600, 500);
		BackgroundImage stars= new BackgroundImage(new Image("Images/sunset2.gif",600,500,false,true),
		BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
		BackgroundSize.DEFAULT);
		root.setBackground(new Background(stars));
		root.getChildren().addAll(titleT, oNames, cPara, face, twit, goog, buttonBack);
		
		this.setCenter(root);
		
		//==========================
		//Button Back
		//==========================
				
				buttonBack.addEventHandler(MouseEvent.MOUSE_CLICKED, 
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonBack.setImage(buttonBackClicked);
								
								Main.mainStage.setScene(new MainMenuScene());
								mediaPlayer.stop();
								
							}
						});
				
				buttonBack.addEventHandler(MouseEvent.MOUSE_ENTERED, 
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonBack.setImage(buttonBackHover);
							}
						});
				
				buttonBack.addEventHandler(MouseEvent.MOUSE_EXITED, 
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonBack.setImage(buttonBackNormal);
							}
						});
	}
	
	
}

package Panes;

import Class.Main;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import Scenes.CreditsScene;
import Scenes.GamesScene;

public class MainMenuPane extends BorderPane{
	public MainMenuPane() {
		
		StackPane buttonOne = new StackPane();
		StackPane buttonTwo = new StackPane();
		StackPane buttonCredits = new StackPane();
		
		Label lb1 = new Label("Games");
		Label lb2 = new Label("Scores");
		Label creditsLabel = new Label("Credits");
		
		Image buttonHover = new Image("Images/ButtonHover.png");
		Image buttonClicked = new Image("Images/ButtonClicked.png");
		Image buttonNormal = new Image("Images/ButtonNormal.png");
		Image buttonExitNormal = new Image("Images/ButtonExit.png");
		Image buttonExitClicked = new Image("Images/ButtonExitClicked.png");
		Image buttonExitHover = new Image("Images/ButtonExitHover.png");
		
		ImageView buttonCardPlay = new ImageView(buttonNormal);
		buttonCardPlay.setFitHeight(55);
		buttonCardPlay.setFitWidth(125);
		
		buttonOne.getChildren().addAll(buttonCardPlay, lb1);
		
		ImageView buttonSpellingPlay = new ImageView(buttonNormal);
		buttonSpellingPlay.setFitHeight(55);
		buttonSpellingPlay.setFitWidth(125);
		
		ImageView creditsButton = new ImageView(buttonNormal);
		creditsButton.setFitHeight(55);
		creditsButton.setFitWidth(125);
		
		buttonCredits.getChildren().addAll(creditsButton, creditsLabel);
		
		ImageView buttonExit = new ImageView(buttonExitNormal);
		buttonExit.setFitHeight(55);
		buttonExit.setFitWidth(55);
		
		buttonTwo.getChildren().addAll(buttonSpellingPlay, lb2);
		
		VBox vbox = new VBox();
		vbox.getChildren().addAll(buttonOne, buttonTwo, buttonCredits);
		vbox.setAlignment(Pos.CENTER);
	    vbox.setSpacing(10);
		
		Pane pane = new Pane();
		pane.getChildren().addAll(vbox, buttonExit);
		vbox.setLayoutX(450);
		vbox.setLayoutY(375);
		buttonExit.setLayoutX(945);
		buttonExit.setLayoutY(695);
	
		
		BackgroundImage farmImage= new BackgroundImage(new Image("Images/farm.png",1000,750,false,true),
		       BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
		        BackgroundSize.DEFAULT);
		
		pane.setBackground(new Background(farmImage));
		
		
		this.setCenter(pane);
		
		//Card Game Button Mouse Events
		buttonOne.addEventHandler(MouseEvent.MOUSE_ENTERED,
		        new EventHandler<MouseEvent>() {
		        	@Override
		        	public void handle(MouseEvent e) {
		        		buttonCardPlay.setImage(buttonHover);
		        	}
		        });
	
		buttonOne.addEventHandler(MouseEvent.MOUSE_EXITED,
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonCardPlay.setImage(buttonNormal);
					}
				});
		buttonOne.addEventHandler(MouseEvent.MOUSE_CLICKED,
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonCardPlay.setImage(buttonClicked);
						Main.mainStage.setScene(new GamesScene());
					}
				});
		
		//Spelling Game Button Mouse Events
		buttonTwo.addEventHandler(MouseEvent.MOUSE_ENTERED,
				        new EventHandler<MouseEvent>() {
				        	@Override
				        	public void handle(MouseEvent e) {
				        		buttonSpellingPlay.setImage(buttonHover);
				        	}
				        });
	
		buttonTwo.addEventHandler(MouseEvent.MOUSE_EXITED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonSpellingPlay.setImage(buttonNormal);
							}
						});
		buttonTwo.addEventHandler(MouseEvent.MOUSE_CLICKED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonSpellingPlay.setImage(buttonClicked);
							}
						});
		
		//Exit Button Mouse Events
		buttonExit.addEventHandler(MouseEvent.MOUSE_ENTERED,
				        new EventHandler<MouseEvent>() {
				        	@Override
				        	public void handle(MouseEvent e) {
				        		buttonExit.setImage(buttonExitHover);
				        	}
				        });
	
		buttonExit.addEventHandler(MouseEvent.MOUSE_EXITED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonExit.setImage(buttonExitNormal);
							}
						});
		buttonExit.addEventHandler(MouseEvent.MOUSE_CLICKED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								buttonExit.setImage(buttonExitClicked);
									System.exit(0);
							}
						});
		
		//Credits Button
		buttonCredits.addEventHandler(MouseEvent.MOUSE_ENTERED,
						new EventHandler<MouseEvent>() {
				        	@Override
				        	public void handle(MouseEvent e) {
				        		creditsButton.setImage(buttonHover);
				        	}
				        });
			
		buttonCredits.addEventHandler(MouseEvent.MOUSE_EXITED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								creditsButton.setImage(buttonNormal);
							}
						});
		
		buttonCredits.addEventHandler(MouseEvent.MOUSE_CLICKED,
						new EventHandler<MouseEvent>() {
							@Override
							public void handle(MouseEvent e) {
								creditsButton.setImage(buttonClicked);
								Main.mainStage.setScene(new CreditsScene());
							}
						});
	}
}

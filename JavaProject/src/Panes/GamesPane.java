package Panes;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import Class.Main;
import Scenes.MainMenuScene;
import Scenes.MatchingGameScene;
import Scenes.SpellingGameScene;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class GamesPane extends BorderPane{
	public GamesPane() {
		
		StackPane buttonOne = new StackPane();
		StackPane buttonTwo = new StackPane();
		StackPane buttonScoreOne = new StackPane();
		StackPane buttonScoreTwo = new StackPane();
		StackPane buttonTutorial1 = new StackPane();
		StackPane buttonTutorial2 = new StackPane();
		
		Label lb1 = new Label("Matching Game");
		Label lb2 = new Label("Spelling Game");
		Label lb3 = new Label("Top Scores");
		Label lb4 = new Label("Top Scores");
		Label lb5 = new Label("Tutorial");
		Label lb6 = new Label("Tutorial");
	
		Image buttonNormal = new Image("Images/ButtonNormal.png");
		Image buttonHover = new Image("Images/ButtonHover.png");
		Image buttonClicked = new Image("Images/ButtonClicked.png");
		Image buttonBackNormal = new Image("Images/ButtonBackNormal.png");
		Image buttonBackClicked = new Image("Images/ButtonBackClicked.png");
		Image buttonBackHover = new Image("Images/ButtonBackHover.png");
		
		ImageView buttonCardStart = new ImageView(buttonNormal);
		buttonCardStart.setFitHeight(55);
		buttonCardStart.setFitWidth(125);
		buttonOne.getChildren().addAll(buttonCardStart, lb1);
		
		ImageView buttonSpellStart = new ImageView(buttonNormal);
		buttonSpellStart.setFitHeight(55);
		buttonSpellStart.setFitWidth(125);
		buttonTwo.getChildren().addAll(buttonSpellStart, lb2);
	
		
		ImageView buttonBack = new ImageView(buttonBackNormal);
		buttonBack.setFitHeight(45);
		buttonBack.setFitWidth(45);
		
		HBox hbox = new HBox();
		hbox.getChildren().addAll(buttonOne, buttonTwo);
		hbox.setLayoutX(365);
		hbox.setLayoutY(350);
		hbox.setSpacing(50);
		
		Pane pane = new Pane();
		pane.getChildren().addAll(hbox, buttonBack);
		
		BackgroundImage farmImage= new BackgroundImage(new Image("Images/farm.png",1000,750,false,true),
		        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
		        BackgroundSize.DEFAULT);
		
		pane.setBackground(new Background(farmImage));
	
		
		this.setCenter(pane);
		
		//Button Event Handlers
		//==========================
		//Card Game Button Events
		//==========================
		
		buttonOne.addEventHandler(MouseEvent.MOUSE_ENTERED, 
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonCardStart.setImage(buttonHover);
					}
				});
		
		buttonOne.addEventHandler(MouseEvent.MOUSE_EXITED,
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonCardStart.setImage(buttonNormal);
					}
				});
		
		buttonOne.addEventHandler(MouseEvent.MOUSE_CLICKED,
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonCardStart.setImage(buttonClicked);
						Main.mainStage.setScene(new MatchingGameScene());
					}
				});
		
		
		
		//==========================
		//Spelling Game Button Events
		//==========================
		
		buttonTwo.addEventHandler(MouseEvent.MOUSE_ENTERED,
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonSpellStart.setImage(buttonHover);
					}
				});
		
		buttonTwo.addEventHandler(MouseEvent.MOUSE_EXITED, 
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonSpellStart.setImage(buttonNormal);
					}
				});
		
		buttonTwo.addEventHandler(MouseEvent.MOUSE_CLICKED, 
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonSpellStart.setImage(buttonClicked);
						Main.mainStage.setScene(new SpellingGameScene());
						FileReader f;
						try {
							f = new FileReader("src/abc.txt");
							int i = f.read();
							while(i != -1) {
								System.out.print((char)i);
								
								i = f.read();
							}
							f.close();
			
						} catch (IOException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						
					}
				});
		
		
		//==========================
		//Button Back
		//==========================
		
		buttonBack.addEventHandler(MouseEvent.MOUSE_CLICKED, 
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonBack.setImage(buttonBackClicked);
						Main.mainStage.setScene(new MainMenuScene());
					}
				});
		
		buttonBack.addEventHandler(MouseEvent.MOUSE_ENTERED, 
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonBack.setImage(buttonBackHover);
					}
				});
		
		buttonBack.addEventHandler(MouseEvent.MOUSE_EXITED, 
				new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent e) {
						buttonBack.setImage(buttonBackNormal);
					}
				});
	}
	
}

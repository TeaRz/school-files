package Panes;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MatchingGamePane extends BorderPane{

	private static final int NUM_PER_ROW = 8;
	private int clickCount = 0;
	private int matchCount = 0;
	private static MediaPlayer mediaPlayer;
	
	private String music = "music.mp3";  
	//https://www.soundjay.com/page-flip-sounds-2.html
	private String cardFlipNoise = "cardFlip.mp3";   
	//https://www.youtube.com/watch?v=DGx9gzCTiu0
	private String matchMade = "match.mp3";  
	//https://www.youtube.com/watch?v=EMYOiK_iQeY
	private String victory = "victory.mp3";  
	// https://www.flickr.com/photos/usdagov/26566295342
	Image backgroundImage = new Image("Images/farm.png");
	
	public MatchingGamePane() {
		
		List<CardTile> imageViews = new ArrayList<CardTile>();
        List<CardTile> selectedCards = new ArrayList<CardTile>();
        Rectangle rect = new Rectangle();
        rect.setOpacity(0);
        rect.setWidth(100);
        rect.setHeight(100);
        //IMAGES FROM: https://www.kenney.nl/assets/animal-pack-redux
        Image cardBack = new Image(getClass().getResourceAsStream("Images/cardBack.png"));
        Image bearImage = new Image(getClass().getResourceAsStream("Images/bear.png"));
        Image frogImage = new Image(getClass().getResourceAsStream("Images/frog.png"));
        Image duckImage = new Image(getClass().getResourceAsStream("Images/duck.png"));
        Image chickenImage = new Image(getClass().getResourceAsStream("Images/chicken.png"));
        Image cowImage = new Image(getClass().getResourceAsStream("Images/cow.png"));
        Image zebraImage = new Image(getClass().getResourceAsStream("Images/zebra.png"));
        Image horseImage = new Image(getClass().getResourceAsStream("Images/horse.png"));
        Image slothImage = new Image(getClass().getResourceAsStream("Images/sloth.png"));
        Image owlImage = new Image(getClass().getResourceAsStream("Images/owl.png"));
        Image chickImage = new Image(getClass().getResourceAsStream("Images/chick.png"));
        Image gorillaImage = new Image(getClass().getResourceAsStream("Images/gorilla.png"));
        Image pigImage = new Image(getClass().getResourceAsStream("Images/pig.png"));
        Image elephantImage = new Image(getClass().getResourceAsStream("Images/elephant.png"));
        Image penguinImage = new Image(getClass().getResourceAsStream("Images/penguin.png"));
        Image monkeyImage = new Image(getClass().getResourceAsStream("Images/monkey.png"));
        Image pandaImage = new Image(getClass().getResourceAsStream("Images/panda.png"));
        Image hippoImage = new Image(getClass().getResourceAsStream("Images/hippo.png"));
        Image giraffeImage = new Image(getClass().getResourceAsStream("Images/giraffe.png"));
        Image rabbitImage = new Image(getClass().getResourceAsStream("Images/rabbit.png"));
        Image snakeImage = new Image(getClass().getResourceAsStream("Images/snake.png"));
        
        imageViews.add(new CardTile(cardBack, pigImage));
        imageViews.add(new CardTile(cardBack, elephantImage));
        imageViews.add(new CardTile(cardBack, pigImage));
        imageViews.add(new CardTile(cardBack, elephantImage));
        imageViews.add(new CardTile(cardBack, penguinImage));
        imageViews.add(new CardTile(cardBack, monkeyImage));
        imageViews.add(new CardTile(cardBack, penguinImage));
        imageViews.add(new CardTile(cardBack, monkeyImage));
        imageViews.add(new CardTile(cardBack, pandaImage));
        imageViews.add(new CardTile(cardBack, hippoImage));
        imageViews.add(new CardTile(cardBack, pandaImage));
        imageViews.add(new CardTile(cardBack, hippoImage));
        imageViews.add(new CardTile(cardBack, giraffeImage));
        imageViews.add(new CardTile(cardBack, rabbitImage));
        imageViews.add(new CardTile(cardBack, giraffeImage));
        imageViews.add(new CardTile(cardBack, rabbitImage));
        imageViews.add(new CardTile(cardBack, snakeImage));
        imageViews.add(new CardTile(cardBack, snakeImage));
        imageViews.add(new CardTile(cardBack, bearImage));
        imageViews.add(new CardTile(cardBack, bearImage));
        imageViews.add(new CardTile(cardBack, zebraImage));
        imageViews.add(new CardTile(cardBack, zebraImage));
        imageViews.add(new CardTile(cardBack, gorillaImage));
        imageViews.add(new CardTile(cardBack, gorillaImage));
        imageViews.add(new CardTile(cardBack, owlImage));
        imageViews.add(new CardTile(cardBack, owlImage));
        imageViews.add(new CardTile(cardBack, slothImage));
        imageViews.add(new CardTile(cardBack, slothImage));
        imageViews.add(new CardTile(cardBack, cowImage));
        imageViews.add(new CardTile(cardBack, cowImage));
        imageViews.add(new CardTile(cardBack, chickenImage));
        imageViews.add(new CardTile(cardBack, chickenImage));
        imageViews.add(new CardTile(cardBack, frogImage));
        imageViews.add(new CardTile(cardBack, frogImage));
        imageViews.add(new CardTile(cardBack, duckImage));
        imageViews.add(new CardTile(cardBack, duckImage));
        imageViews.add(new CardTile(cardBack, horseImage));
        imageViews.add(new CardTile(cardBack, horseImage));
        imageViews.add(new CardTile(cardBack, chickImage));
        imageViews.add(new CardTile(cardBack, chickImage));
        
        
        Collections.shuffle(imageViews);
        
        GridPane rootNode = new GridPane();
        BorderPane bordPane = new BorderPane();
		
		
		bordPane.setCenter(rootNode);
		bordPane.setBackground(new Background(new BackgroundImage(backgroundImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, null)));
       
        for(int i = 0; i < imageViews.size(); i++) {
            CardTile card = imageViews.get(i);
            card.setTranslateX(150 * (i % NUM_PER_ROW));
            card.setTranslateY(200 * (i / NUM_PER_ROW));
            rootNode.getChildren().add(card);
        }
        

        
        
        
        for(CardTile view : imageViews) {
        	
            view.setOnMouseClicked(e2 -> {
            	if(clickCount ==2) {
            		return;
            	}
            	//play audio for clicking the card.
            	setMedia(cardFlipNoise);
            	
                if(!view.cardUp()) {
                    view.showCardFront();
                    selectedCards.add(view);
                    if(selectedCards.size() == 2) {
                    	clickCount = 2;
                        if(selectedCards.get(0).getImage().equals(selectedCards.get(1).getImage())) {
                            //play audio clip for match, and increment score counter
                        	setMedia(matchMade);
                            matchCount++;
                            if(matchCount == 20) {
                            	setMedia(victory);
                            	bordPane.setCenter(rect);
                            	setMedia(music);
                            	Timeline delay2 = new Timeline(new KeyFrame(Duration.seconds(5.0), (e3 -> {
                                })));
                            	delay2.play();
                            	delay2.setOnFinished(e4 -> {
                                	//restart(stage);  
                            	});
                            	
                            }
                            //clear list and click count for next 2 clicks
                            selectedCards.clear();
                            clickCount = 0;
                        }
                        else {
                            Timeline delay = new Timeline(new KeyFrame(Duration.seconds(0.75), (e5 -> {
                            	//flip the cards back over   
                            })));
                            delay.play();
                            delay.setOnFinished(e5 ->{
                            	selectedCards.get(0).showCardBack();
                            	selectedCards.get(1).showCardBack(); 
                            	//clear list and click count for next 2 clicks
                            	selectedCards.clear();
                            	clickCount = 0;
                            });                      
                        }
                    } 
                }                
            });
        }
        
        this.setCenter(bordPane);
	}
	
    public void setMedia(String path) {
        mediaPlayer = new MediaPlayer(new Media(new File(path).toURI().toString()));
        mediaPlayer.play();
    }

}

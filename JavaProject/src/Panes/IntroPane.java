package Panes;

import java.io.File;

import Class.Main;
import Scenes.MainMenuScene;
import Scenes.SpellingGameScene;
import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public class IntroPane extends BorderPane{

	public IntroPane() {
		StackPane playButton = new StackPane();
		StackPane exitButton = new StackPane();
		String musicFile = "src/Music/thebees.wav";
		String music2 = "src/Music/opening.wav";		
		Label playL = new Label("Play");
		Label exitL = new Label("Exit");
		//Image views
		Image bee = new Image("Images/bee.png", true);
		ImageView iv = new ImageView(bee);
		Image title = new Image("Images/titleText.png", true);
		ImageView titleT = new ImageView(title);
		titleT.setFitHeight(100);
		titleT.setFitWidth(600);
		titleT.setLayoutX(0);
		titleT.setLayoutY(150);
		Image button = new Image("Images/Button_05.png", true);
		Image buttonHover = new Image("Images/Button_06.png", true);
		Image exit = new Image("Images/Button_07.png", true);
		Image exitHover = new Image("Images/exitButton.png", true);
		Image facebook = new Image("Images/facebook.png", true);
		Image twitter = new Image("Images/twitter.png", true);
		Image google = new Image("Images/google.png", true);
		
		ImageView buttonPlay = new ImageView(button);
		buttonPlay.setFitHeight(60);
		buttonPlay.setFitWidth(100);
		playButton.getChildren().addAll(buttonPlay, playL);
	    playButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
	            new EventHandler<MouseEvent>() {
	                @Override
	                public void handle(MouseEvent e) {
	                    buttonPlay.setImage(buttonHover);
	                }
	            });
	    playButton.addEventHandler(MouseEvent.MOUSE_EXITED,
	            new EventHandler<MouseEvent>() {
	                @Override
	                public void handle(MouseEvent e) {
	                    buttonPlay.setImage(button);
	                    Main.mainStage.setScene(new MainMenuScene());
	                }
	            });
		ImageView buttonExit = new ImageView(exit);
		buttonExit.setFitHeight(60);
		buttonExit.setFitWidth(100);
		exitButton.getChildren().addAll(buttonExit, exitL);
	    exitButton.addEventHandler(MouseEvent.MOUSE_ENTERED,
	            new EventHandler<MouseEvent>() {
	                @Override
	                public void handle(MouseEvent e) {
	                    buttonExit.setImage(exitHover);
	                }
	            });
	    exitButton.addEventHandler(MouseEvent.MOUSE_EXITED,
	            new EventHandler<MouseEvent>() {
	                @Override
	                public void handle(MouseEvent e) {
	                    buttonExit.setImage(exit);
	                }
	            });
	    exitButton.addEventHandler(MouseEvent.MOUSE_CLICKED,
	            new EventHandler<MouseEvent>() {
	                @Override
	                public void handle(MouseEvent e) {
	                    buttonExit.setImage(exitHover);
	                   System.exit(0);
	                }
	            });
	
		//Facebook
		Button face = new Button();
		BackgroundImage biFace = new BackgroundImage(facebook, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(facebook.getWidth(), facebook.getHeight(), true, true, true, false));
	    Background backFace = new Background(biFace);
	    face.setMinSize(80, 37);
	    face.setMaxSize(80, 37);
		face.setLayoutX(500);
		face.setLayoutY(20);
		face.setBackground(backFace);
		//Twitter
		Button twit = new Button();
		BackgroundImage biTwit = new BackgroundImage(twitter, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(twitter.getWidth(), twitter.getHeight(), true, true, true, false));
	    Background backTwit = new Background(biTwit);
	    twit.setMinSize(80, 37);
	    twit.setMaxSize(80, 37);
		twit.setLayoutX(460);
		twit.setLayoutY(20);
		twit.setBackground(backTwit);
		//Google
		Button goog = new Button();
		BackgroundImage biGoog = new BackgroundImage(google, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(google.getWidth(), google.getHeight(), true, true, true, false));
	    Background backGoog = new Background(biGoog);
	    goog.setMinSize(80, 37);
	    goog.setMaxSize(80, 37);
		goog.setLayoutX(540);
		goog.setLayoutY(20);
		goog.setBackground(backGoog);
		
		Media sound = new Media(new File(musicFile).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(sound);
		mediaPlayer.play();
		
		Media sound2 = new Media(new File(music2).toURI().toString());
		MediaPlayer mediaPlayer2 = new MediaPlayer(sound2);
		mediaPlayer2.play();
		VBox buttons = new VBox();
		buttons.getChildren().addAll(playButton, exitButton);
		buttons.setAlignment(Pos.CENTER);
		buttons.setSpacing(10);
		//Animations
		FadeTransition fade = new FadeTransition(Duration.millis(2000), titleT);
		fade.setFromValue(1);
		fade.setToValue(0);
		fade.setCycleCount(PathTransition.INDEFINITE);
		fade.setAutoReverse(true);
		fade.play();
		
	    ScaleTransition scale = new ScaleTransition(Duration.millis(3000), titleT);  
	    scale.setByX(1.5f);  
	    scale.setByY(1.2f);  
	    scale.setCycleCount(2);  
	    scale.setAutoReverse(true); 
	    scale.play();
		
		//Translate Transition to move the bee 
		TranslateTransition transition = new TranslateTransition();
		transition.setDuration(Duration.seconds(5));
		transition.setToX(500);
		transition.setToY(500);
		transition.setNode(iv);
		transition.play();
		Pane root = new Pane();
		root.getChildren().addAll(buttons, titleT, iv, face, twit, goog);
		Scene scene = new Scene(root, 600, 500);
		buttons.setLayoutX(250);
		buttons.setLayoutY(350);
		BackgroundImage farmImage= new BackgroundImage(new Image("https://image.freepik.com/free-vector/farm-with-barn_1196-526.jpg",600,500,false,true),
		BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
		BackgroundSize.DEFAULT);
		root.setBackground(new Background(farmImage));
		this.setCenter(root);
	}
}

import java.io.File;

public class TestFile {

	public static void main(String[] args) {
		//Created a file object that points at a file
		File file = new File("Music/Oomph.mp3");
		//Output all the properties of the file
		System.out.println("Does it exist? " +
		file.exists());
		System.out.println("The file has " +
		file.length() + " bytes");
		System.out.println("Can it be read? " +
		file.canRead());
		System.out.println("Can it be written to? " +
				file.canWrite());
		System.out.println("Is it a file? " +
				file.isFile());
		System.out.println("Is it a directory? " +
				file.isDirectory());
		System.out.println("Is it a hidden file? " +
				file.isHidden());
		System.out.println("When was the file last modified? " +
				file.lastModified());
	}

}

import java.io.File;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class MyMusicPlayer extends Application{

	MediaPlayer player = null;
	static Media media = null;
	ImageView play;
	
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//Create a file object pointing at a folder that contains the music
		File folder = new File("Music");
		//Create a list of files that are the files in the folder
		File[] list = folder.listFiles(); 
		//Create an ArrayList to store the names of all the files populate the array with file names
		ArrayList<String> fileNames = new ArrayList<String>();
		for(File file: list) {
			if(file.isFile()) {
				fileNames.add(file.getName());
			}
		}
		//Turn the ArrayList into an observable array
		ObservableList<String> songs = 
				(ObservableList<String>) 
				FXCollections.observableArrayList(fileNames);
		//Create a BorderPane
		BorderPane root = new BorderPane();
		//Create a ListView
		ListView listOfSongs = new ListView();
		//populate the ListView
		listOfSongs.setItems(songs);
		listOfSongs.setMaxHeight(200);
		listOfSongs.setPrefWidth(300);
		//Add the ListView to the BorderPane
		root.setLeft(listOfSongs);
		HBox controls = new HBox();
		controls.setAlignment(Pos.CENTER);
		//Create both stop and play images
		play = new ImageView(new Image("play.png"));
		play.setFitHeight(100);
		play.setFitWidth(100);
		ImageView stop = new ImageView(new Image("stop.png"));
		stop.setFitHeight(100);
		stop.setFitWidth(100);
		//Add them to an HBox and place the HBox in the pane
		controls.getChildren().addAll(play, stop);
		controls.setSpacing(10);
		root.setCenter(controls);
		//Create events for the play and stop images	
		play.setOnMouseClicked(e->{
			// If no file is selected; return
			if (listOfSongs.getSelectionModel().isEmpty()) return; 
			
			if(player == null) {  //if we have a mediaplayer 
				// Grab the selected file in the ListView
				// Build a File "Music/songname.mp3"
				// Grab the Files URI and providing that as a parameter
				media = new Media(new File("Music/"
						+ listOfSongs.
						getSelectionModel().
						getSelectedItem().toString()).
						toURI().toString());
				player = new MediaPlayer(media); 
				play.setImage(new Image("pause.png"));
				player.play();
			} else { //player has never been constructed
				switch(player.getStatus()) {
				case PAUSED:  //Music is currently paused
					play.setImage(new Image("pause.png")); // Switch image to indicate pause 
					player.play(); //Play the music
					break;
				case PLAYING: //Music is playing
					play.setImage(new Image("play.png")); // Switch the image to indicate play
					player.pause(); //Pause the music
					break;
				case STOPPED: //Music is stopped
					media = new Media(new File("Music/"
							+ listOfSongs.
							getSelectionModel().
							getSelectedItem().toString()).
							toURI().toString()); //Select new file
					player = new MediaPlayer(media); // Select new source
					play.setImage(new Image("pause.png")); // Switch the image to indicate pause 
					player.play(); //Play the music
					break;
				default:
					break;
				}
			}
		});
		
		stop.setOnMouseClicked(e->{
			//If you have a media player
			if(player != null && player.getStatus() == MediaPlayer.Status.PLAYING) {
					player.stop();
					play.setImage(new Image("play.png"));
			}
		});
		
		
		Scene scene = new Scene(root, 750, 200);
		primaryStage.setScene(scene);
		primaryStage.setTitle("OSMediaPlayer");
		primaryStage.setResizable(false);
		primaryStage.show();
	}

}
